#!/bin/bash

set -e

ROOT_DIR=$(pwd)
TMP_DIR=$(mktemp -d 2>/dev/null || mktemp -d -t 'mytmpdir')
SOURCE_DIR=${ROOT_DIR}/source/
TARGET_DIR=${ROOT_DIR}/target/
LIMIT=$1

echo 'Temporary Dir is ' ${TMP_DIR}
echo 'Limit is ' ${LIMIT}

TARGET_DIR_NL=${TARGET_DIR}nl/
TARGET_DIR_FR=${TARGET_DIR}fr/

LESSC=./node_modules/.bin/lessc
POSTCSS=./node_modules/.bin/postcss

# init
mkdir -p ${TARGET_DIR_NL}
mkdir -p ${TARGET_DIR_FR}
mkdir -p ${TARGET_DIR_NL}/atlas
mkdir -p ${TARGET_DIR_FR}/atlas

# style
mkdir -p ${TARGET_DIR}style
cp -a ${SOURCE_DIR}styles/svg ${TARGET_DIR}style
cp -a ${SOURCE_DIR}styles/fonts ${TARGET_DIR}style
${LESSC} ${SOURCE_DIR}styles/less/style.less ${TMP_DIR}/style.css
${POSTCSS} ${TMP_DIR}/style.css \
  -o ${TARGET_DIR}style/style.css \
  --use autoprefixer


#files & medias
${ROOT_DIR}/scripts/img-pyramid.sh ${SOURCE_DIR}images ${TARGET_DIR}images
cp -a ${SOURCE_DIR}audio ${TARGET_DIR}
cp -a ${SOURCE_DIR}files ${TARGET_DIR}


# landing page
${ROOT_DIR}/scripts/index.py ${SOURCE_DIR}collection/source ${SOURCE_DIR}fr/index.html ${TARGET_DIR_FR}index.html
${ROOT_DIR}/scripts/index.py ${SOURCE_DIR}collection/source ${SOURCE_DIR}nl/index.html ${TARGET_DIR_NL}index.html
cp ${SOURCE_DIR}fr/about.html ${TARGET_DIR_FR}about.html
cp ${SOURCE_DIR}nl/about.html ${TARGET_DIR_NL}about.html
cp -a ${SOURCE_DIR}images ${TARGET_DIR}

# planche_bota
mkdir -p ${TARGET_DIR}/images/botanic
cp ${SOURCE_DIR}/collection/source/planche_botanique/* ${TARGET_DIR}images/botanic


# collection fr
${ROOT_DIR}/scripts/gen.py \
  ${SOURCE_DIR}/collection/source \
  ${SOURCE_DIR}/collection/templates/fiche-fr.html \
  ${TARGET_DIR_FR}/atlas \
  ${LIMIT}

# collection nl
${ROOT_DIR}/scripts/gen.py \
  ${SOURCE_DIR}/collection/source \
  ${SOURCE_DIR}/collection/templates/fiche-nl.html \
  ${TARGET_DIR_NL}/atlas \
  ${LIMIT}


# map
${ROOT_DIR}/scripts/geojson.py \
  ${SOURCE_DIR}/collection/source \
  ${ROOT_DIR}/ts/src

( cd ${ROOT_DIR}/ts && npm install && node webpack.js )

cp ${ROOT_DIR}/ts/dist/bundle.js \
  ${ROOT_DIR}/ts/dist/bundle.js.map  \
  ${TARGET_DIR_FR}/atlas/

cp ${ROOT_DIR}/ts/dist/bundle.js \
  ${ROOT_DIR}/ts/dist/bundle.js.map  \
  ${TARGET_DIR_NL}/atlas/

cp ${SOURCE_DIR}fr/map.html ${TARGET_DIR_FR}atlas/index.html
cp ${SOURCE_DIR}nl/map.html ${TARGET_DIR_NL}atlas/index.html


#print
phantomjs scripts/gen_pdf.js ${TARGET_DIR_FR}atlas/id_*.html
phantomjs scripts/gen_pdf.js ${TARGET_DIR_NL}atlas/id_*.html

# cleanup
rm -rf ${TMP_DIR}
