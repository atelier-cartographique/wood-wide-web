
import {
    Collection,
    extent,
    Feature,
    format,
    geom,
    // Graticule,
    interaction,
    layer,
    Map,
    Overlay,
    proj,
    render,
    source,
    style,
    View,
} from 'openlayers';

import * as proj4 from 'proj4';
import { FeatureCollection } from "geojson-iots";

proj.setProj4(proj4);
proj4.defs('EPSG:31370',
    '+proj=lcc +lat_1=51.16666723333333 +lat_2=49.8333339 +lat_0=90 +lon_0=4.367486666666666 +x_0=150000.013 +y_0=5400088.438 +ellps=intl +towgs84=-106.8686,52.2978,-103.7239,-0.3366,0.457,-1.8422,-1.2747 +units=m +no_defs');

type Lang = 'fr' | 'nl';
const lang: Lang = (<any>window).woodLang;

interface TrRecord {
    'fr': string;
    'nl': string;
}

type RecordKey =
    | 'fullNameField'
    | 'latinNameField'
    | 'familyNameField'
    | 'addressField'
    | 'fileName'
    | 'toPage'
    ;

type RecordDB = {
    [k in RecordKey]: TrRecord;
}

const records: RecordDB = {
    fullNameField: {
        fr: 'nom_fr',
        nl: 'nom_nl',
    },
    latinNameField: {
        fr: 'latin',
        nl: 'latin',
    },
    familyNameField: {
        fr: 'famille_fr',
        nl: 'famille_nl',
    },
    addressField: {
        fr: 'adresse_1',
        nl: 'adres_1',
    },
    fileName: {
        fr: 'wood_file',
        nl: 'wood_file',
    },
    toPage: {
        fr: 'Voir la fiche',
        nl: 'Zie pagina',
    }
};


const getRecord =
    (k: RecordKey) => records[k][lang];


const getFromProps =
    (k: RecordKey) =>
        <T=string>(props: any, defaultValue: T) => {
            const tk = records[k][lang];
            if (props && tk in props) {
                return props[tk];
            }
            return defaultValue;
        };

const getName = getFromProps('fullNameField');
const getSpecie = getFromProps('latinNameField');
const getFamily = getFromProps('familyNameField');
const getAddress = getFromProps('addressField');
const getFileName = getFromProps('fileName');



const EPSG31370 = new proj.Projection({
    code: 'EPSG:31370',
    extent: [14697.30, 22635.80, 291071.84, 246456.18],
    worldExtent: [2.5, 49.5, 6.4, 51.51],
    global: false,
    getPointResolution: (r: number) => r,
});

proj.addProjection(EPSG31370);

const wmsOrthoConfig = {
    srs: 'EPSG:31370',
    params: {
        LAYERS: 'Urbis:Ortho2017',
        VERSION: '1.1.1',
    },
    url: 'https://geoservices-urbis.irisnet.be/geoserver/ows'
};
// const wmsOrthoConfig = {
//     srs: 'EPSG:31370',
//     params: {
//         LAYERS: 'wood-wide-web',
//         VERSION: '1.3.0',
//     },
//     url: 'http://127.0.0.1:2000/wms'
//     // url: 'http://172.17.0.1:8080/'
// };

const jsonFormat = new format.GeoJSON();

// const bufferFeature =
//     (f: Feature, n: number) => {
//         const g = f.getGeometry();
//         const poly = polygon(g.coo);
//         const scaledPoly = transformScale(gj, n);
//     }

const Styles = {
    'rmqb':
        (_f: Feature) => [new style.Style({
            stroke: new style.Stroke({
                width: 3,
                color: '#ff0000',
            })
        })],
    'remarquableAvecPortrait':
        (f: Feature) => [
            new style.Style({
                stroke: new style.Stroke({
                    width: 3,
                    color: '#ff0000',
                })
            }),
            new style.Style({
                geometry: () => {
                    const poly = f.clone().getGeometry() as geom.Polygon
                    const g = new geom.Polygon(poly.getCoordinates());
                    g.scale(5, 5);
                    return g;
                },
                stroke: new style.Stroke({
                    width: 4,
                    color: '#0090e4',
                }),
            })],
    'rmqe':
        (_f: Feature) => [new style.Style({
            stroke: new style.Stroke({
                width: 3,
                color: '#ffbc00',
            })
        })],
}

type StyleMap = typeof Styles;
type StyleKey = keyof StyleMap;

const makeDefaultStyle =
    () =>
        (f: Feature) => {
            const props = f.getProperties();
            let cat: StyleKey = props['category'];
            if ('has_portrait' in props && props['has_portrait']) {
                cat = 'remarquableAvecPortrait';
            }
            return Styles[cat](f);
        }

// const makeHighlightStyle =
//     () => new style.Style({
//         fill: new style.Fill({
//             color: '#0094F5',
//         }),
//         stroke: new style.Stroke({
//             width: 2,
//             color: 'white',
//         })
//     })

const makeHighlightStyle =
    () => [
        new style.Style({
            stroke: new style.Stroke({
                width: 2,
                color: 'red',
            })
        }),
        new style.Style({
            geometry: (f) =>
                new geom.Point(extent.getCenter(f.getGeometry().getExtent())),
            image: new style.Circle({
                radius: 5,
                fill: new style.Fill({
                    color: '#0094F5',
                }),
                stroke: new style.Stroke({
                    width: 2,
                    color: 'white',
                })
            })
        })]

const boxedText =
    (t: string, className: string) => {
        const e = document.createElement('div')
        e.setAttribute('class', className)
        e.appendChild(document.createTextNode(t))
        return e
    }

const boxedAnchor =
    (t: string, href: string, className: string) => {
        const e = document.createElement('div')
        const a = document.createElement('a')
        e.setAttribute('class', className)
        a.setAttribute('href', href)
        a.appendChild(document.createTextNode(t))
        e.appendChild(a);
        return e
    }

const makeMap =
    () => {
        // const target = document.getElementById('map')
        const view = new View({
            projection: EPSG31370,
            center: [148651, 170897],
            rotation: 0,
            zoom: 6,
        });
        const map = new Map({
            target: 'map',
            view,
        });



        const layers: layer.Vector[] = [];
        const baseLayer = new layer.Tile({
            source: new source.TileWMS({
                projection: proj.get(wmsOrthoConfig.srs),
                params: {
                    ...wmsOrthoConfig.params,
                    'TILED': true,
                },
                url: wmsOrthoConfig.url,
            }),
        });

        interface Popup {
            id: string;
            o: Overlay;
        }
        let popup: Popup | null = null;

        map.addLayer(baseLayer);


        const makePopup =
            (f: Feature | render.Feature, props: any) => {
                const element = document.createElement('div');
                const e = f.getGeometry().getExtent()
                const position: [number, number] = extent.getTopLeft(e);
                element.setAttribute('class', 'popup');

                element.appendChild(boxedText(getName(props, ''), 'name'))
                element.appendChild(boxedText(getAddress(props, ''), 'address'))
                element.appendChild(boxedText(getSpecie(props, ''), 'specie'))
                element.appendChild(boxedAnchor(
                    getRecord('toPage'), getFileName(props, ''), 'link'))

                return {
                    id: props.id,
                    o: new Overlay({
                        element,
                        position,
                        positioning: 'bottom-left',
                        offset: [0, -16],
                        autoPan: true,
                    })
                }
            }

        // map.on('pointermove', (e: MapBrowserEvent) => {
        //     const fs = map.getFeaturesAtPixel(e.pixel);
        //     if (fs && fs.length > 0) {
        //         const props = fs[0].getProperties();
        //         if (props) {
        //             console.log(Object.keys(props))
        //             if (popup && popup.id !== props.id) {
        //                 map.removeOverlay(popup.o);
        //                 popup = null;
        //             }
        //             if (popup === null) {
        //                 popup = makePopup(fs[0], props)
        //                 map.addOverlay(popup.o)
        //             }
        //         }
        //     }
        //     else if (popup !== null) {
        //         map.removeOverlay(popup.o);
        //         popup = null;
        //     }
        // });

        const select = new interaction.Select({
            style: makeHighlightStyle()
        })
        select.on('select', (e: any) => {
            const fs: Collection<Feature> = e.target.getFeatures();
            const f = fs.getLength() > 0 ? fs.item(0) : null;
            if (f) {
                const props = f.getProperties();
                if (props) {
                    // console.log(Object.keys(props))
                    if (popup && popup.id !== props.id) {
                        map.removeOverlay(popup.o);
                        popup = null;
                    }
                    if (popup === null) {
                        popup = makePopup(f, props)
                        map.addOverlay(popup.o)
                    }
                }
            }
            else if (popup !== null) {
                map.removeOverlay(popup.o);
                popup = null;
            }
        });

        map.addInteraction(select);

        const addLayer =
            (name: string, features: Feature[]) => {
                const woodSource = new source.Vector();
                const woodLayer = new layer.Vector({
                    source: woodSource,
                    style: makeDefaultStyle(),
                });
                woodLayer.set('name', name);
                layers.push(woodLayer);
                woodSource.addFeatures(features);
                map.addLayer(woodLayer);
            }

        const visible =
            (layerNames: string[]) =>
                layers.forEach(
                    l => l.setVisible(layerNames.indexOf(l.get('name')) >= 0));

        const highlight =
            (layerNames: string[]) => {
                layers.forEach(l => l.setStyle(makeDefaultStyle()));
                layers.forEach(
                    (l) => {
                        const name = l.get('name');
                        if (layerNames.indexOf(name) >= 0) {
                            console.log(`highlight ${name} ${layerNames}`)
                            l.setStyle(makeHighlightStyle());
                        }
                    });
            }


        // const graticule = new Graticule({
        //     strokeStyle: new style.Stroke({
        //         color: 'rgba(255,120,0,0.6)',
        //         width: 0.5,
        //         // lineDash: [0.5, 4]
        //     }),
        // });
        // graticule.setMap(map);

        return {
            addLayer,
            visible,
            highlight,
            map,
        }
    }

type ToggleListener = (a: boolean) => void;

class Toggler {
    private value = false;
    private listeners: ToggleListener[] = [];

    constructor(private name: string) { }

    toggle() {
        this.value = !this.value;
        this.listeners.forEach(f => f(this.value));
        return this.value;
    }

    concat(xs: string[]) {
        if (this.value) {
            return xs.concat([this.name])
        }
        return xs
    }

    onToggle(cb: ToggleListener) {
        this.listeners.push(cb)
    }
}

const makeFilterItem =
    (name: string) => {
        const node = document.createElement('div');
        node.setAttribute('class', 'legend-item');
        const toggle = new Toggler(name)

        node.innerText = name;
        node.addEventListener('click',
            () => {
                const togState = toggle.toggle();
                if (togState) {
                    node.setAttribute('class', 'legend-item highlight');
                }
                else {
                    node.setAttribute('class', 'legend-item');
                }
            }, false)

        return { node, toggle };
    }



const start =
    () => {
        const { addLayer, highlight } = makeMap();
        const speciesSet = new Set<string>();
        fetch('/atlas/data/')
            .then(response => response.json())
            .then((geoData: FeatureCollection) => {
                geoData.features
                    .map(f => getFamily(f.properties, ''))
                    .filter(name => name !== '')
                    .forEach(name => speciesSet.add(name));
                const species = Array.from(speciesSet.values()).sort()

                species.forEach(name => addLayer(name,
                    geoData.features
                        .filter(f => getFamily(f.properties, '') === name)
                        // .map(f => {
                        //     console.log(`[${name}] ${getFamily(f.properties, false)}`);
                        //     return f;
                        // })
                        .map(f => jsonFormat.readFeature(f))));


                const filterElem = document.getElementById('specie-filter');
                const toggles: Toggler[] = [];
                const toggleHandler =
                    () => highlight(toggles.reduce((acc, t) => t.concat(acc), []));

                if (filterElem) {
                    species.forEach((name) => {
                        const { node, toggle } = makeFilterItem(name);
                        toggle.onToggle(toggleHandler);
                        toggles.push(toggle);
                        filterElem.appendChild(node);
                    })
                }

                const legendElem = document.querySelector('.legend-map');

                if (legendElem) {
                }
            })
    }


document.onreadystatechange = function startApplication() {
    if ('interactive' === document.readyState) {
        start();
    }
};