from django.contrib.admin import site, ModelAdmin, SimpleListFilter, StackedInline
from easy_thumbnails.widgets import ImageClearableFileInput
from easy_thumbnails.fields import ThumbnailerImageField

from .models.species import Taxon, TaxonQuality, TaxonPicture


class TaxonQualityAdmin(ModelAdmin):
    search_fields = (
        'value__fr',
        'value__en',
        'value__nl',
    )


class PictureWidget(ImageClearableFileInput):
    def render(self, name, value, attrs=None, renderer=None):
        return super().render(name, value, attrs)


class TaxonPictureInline(StackedInline):
    formfield_overrides = {ThumbnailerImageField: dict(widget=PictureWidget)}
    model = TaxonPicture
    autocomplete_fields = search_fields = ('caption', )


class TaxonAdmin(ModelAdmin):
    list_display = ('genus', 'species', 'name')
    inlines = (TaxonPictureInline, )
    autocomplete_fields = search_fields = (
        'name',
        'origin',
        'soil',
        'climate',
        'beauty',
        'biodiversity',
        'oxygeny',
        'purify',
        'filtery',
        'floody',
        'carbony',
        'sweety',
        'soily',
        'healthy',
    )


site.register(TaxonQuality, TaxonQualityAdmin)
site.register(Taxon, TaxonAdmin)
site.register(TaxonPicture)
