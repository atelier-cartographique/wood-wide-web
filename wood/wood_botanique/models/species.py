from django.db import models
from easy_thumbnails.fields import ThumbnailerImageField

from wood_lingua.models import message_field
from wood_core.models.media import picture


class TaxonQuality(models.Model):
    """Models a quantified quality of a taxon
    """
    id = models.AutoField(primary_key=True)

    value = message_field('taxon_quality')
    quantity = models.IntegerField()

    def __str__(self):
        return '{} ({})'.format(self.value, self.quantity)


def quality_field(name):
    return models.ForeignKey(
        TaxonQuality,
        on_delete=models.CASCADE,
        related_name=name,
    )


class TaxonManager(models.Manager):
    def get_queryset(self):
        select = (
            'name',
            'origin',
            'soil',
            'climate',
            'beauty',
            'biodiversity',
            'oxygeny',
            'purify',
            'filtery',
            'floody',
            'carbony',
            'sweety',
            'soily',
            'healthy',
        )
        return super().get_queryset().select_related(*select)

    def find(self, latin_name):
        genus = latin_name.split(' ')[0]
        species = ' '.join(latin_name.split(' ')[1:])

        return self.get_queryset().get(genus=genus, species=species)


class Taxon(models.Model):
    """Models a taxon
    """
    id = models.AutoField(primary_key=True)
    objects = TaxonManager()

    family = models.CharField(max_length=64)
    genus = models.CharField(max_length=64)
    species = models.CharField(max_length=64)

    trivia = message_field('taxon_trivia')
    name = message_field('taxon_name')

    origin = message_field('taxon_origin')
    soil = message_field('taxon_soil')
    climate = message_field('taxon_climate')

    max_age = message_field('taxon_max_age')
    max_height = message_field('taxon_max_height')
    max_circumference = message_field('taxon_max_circumference')

    beauty = quality_field('taxon_beauty')
    biodiversity = quality_field('taxon_biodiversity')
    oxygeny = quality_field('taxon_oxygeny')
    purify = quality_field('taxon_purify')
    filtery = quality_field('taxon_filtery')
    floody = quality_field('taxon_floody')
    carbony = quality_field('taxon_carbony')
    sweety = quality_field('taxon_sweety')
    soily = quality_field('taxon_soily')
    healthy = quality_field('taxon_healthy')

    def __str__(self):
        return '{} {}'.format(self.genus, self.species)


TaxonPicture = picture(Taxon)