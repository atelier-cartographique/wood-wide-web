from django.apps import AppConfig


class WoodBotaniqueConfig(AppConfig):
    name = 'wood_botanique'
