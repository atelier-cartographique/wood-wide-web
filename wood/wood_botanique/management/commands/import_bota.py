#  Copyright (C) 2018 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from pathlib import Path
from csv import DictReader
import re
from collections import namedtuple
import networkx as nx
from uuid import uuid4

from django.core.management.base import BaseCommand, CommandError

from wood_botanique.models.species import Taxon, TaxonQuality, TaxonPicture
from wood_lingua.models import message, simple_message

## << graph


def get_objects(node, pred):
    ks = node.keys()
    results = []
    for k in ks:
        np = node[k]['p']
        if np == pred:
            results.append(val(k))

    return results


def get_first_object(node, pred, default=''):
    results = get_objects(node, pred)
    if len(results) > 0:
        return results[0]

    return default


Obj = namedtuple('Obj', ['id', 'value'])


def obj(val):
    return Obj(uuid4(), val)


def val(o):
    return o.value


## graph >>


def get_planche_bota(G, taxon, name):
    results = []
    try:
        node = G[name]
        img_names = get_objects(node, 'planche_botanique')
        for name in img_names:
            if name is not '':
                label_fr = get_first_object(G[name], 'caption fr')
                label_nl = get_first_object(G[name], 'caption nl')
                TaxonPicture.objects.create(
                    linked_object=taxon,
                    file='wood_botanique.taxon/{}'.format(name),
                    caption=message(
                        'picture_caption_taxon',
                        fr=label_fr,
                        nl=label_nl,
                        en=''))
    except Exception:
        print('Failed getting planche_bota for {}'.format(name))


def latin(row):
    return row['Latin']


def family(row):
    return row['Famille']


def genus(row):
    return latin(row).split(' ')[0]


def species(row):
    return ' '.join(latin(row).split(' ')[1:])


def name(row):
    return message(
        'taxon_name',
        fr=row['Nom FR'],
        nl=row['Nom NL'],
        en=row['Nom EN'],
    )


def trivia(row):
    return message(
        'taxon_trivia',
        fr=row['Famille FR'],
        nl=row['Famille NL'],
        en=row['Famille'],
    )


def origin(row):
    return message(
        'taxon_origin',
        fr=row['Origine / Indigène'],
        nl=row['Oorsprong/Afkomst'],
        en='',
    )


def soil(row):
    return message(
        'taxon_soil',
        fr=row['Sol préféré'],
        nl=row['Voorkeursbodem'],
        en='',
    )


def climate(row):
    return message(
        'taxon_climate',
        fr=row['Climat préféré'],
        nl=row['Voorkeursklimaat'],
        en='',
    )


def max_age(row):
    return message(
        'taxon_max_age',
        fr=row['Longévité espérée'],
        nl=row['Verwachte levensduur'],
        en='')


def max_height(row):
    return message(
        'taxon_max_height',
        fr=row['Hauteur visée'],
        nl=row['Beoogde hoogte'],
        en='')


def max_circumference(row):
    return message(
        'taxon_max_circumference',
        fr=row['Circonférence espérée'],
        nl=row['Verwachte omtrek'],
        en='')


# KKK (nuttige soort voor een hele reeks insecten en vogels)
extractKKtruc_re = re.compile(r"^(.+) \(.+")


def get_quality_score(s):
    # print('getscore {}, {},{}'.format(this, options, s))
    score = 0
    m = re.match(extractKKtruc_re, s)
    if m is None:
        return score

    g, = m.groups()
    c = g[0]
    if 'K' == c:
        score = len(g)

    elif '/' == c:
        score = -1 * len(g)

    return score


extractKKbidule_re = re.compile(r".+ \((.+)\)")


def get_quality_label(s):
    # print('get_quality_label({})'.format(s))
    Klabel = 'ø'
    m = re.match(extractKKbidule_re, s)
    if m is None:
        return Klabel

    g, = m.groups()
    return g


def create_quality(quantity, fr, nl, en):
    return TaxonQuality.objects.create(
        quantity=quantity,
        value=message('taxon_quality', fr, nl, en),
    )


def beauty(row):
    k_fr = 'Embellit le paysage'
    k_nl = 'Verfraait het landschap'
    return create_quality(
        get_quality_score(row[k_fr]),
        get_quality_label(row[k_fr]),
        get_quality_label(row[k_nl]),
        en='')


def biodiversity(row):
    k_fr = 'Enrichit la biodiversité'
    k_nl = 'Verrijkt de biodiversiteit'
    return create_quality(
        get_quality_score(row[k_fr]),
        get_quality_label(row[k_fr]),
        get_quality_label(row[k_nl]),
        en='')


def oxygeny(row):
    k_fr = 'Fournit de l\'oxygène'
    k_nl = 'Levert zuurstof'
    return create_quality(
        get_quality_score(row[k_fr]),
        get_quality_label(row[k_fr]),
        get_quality_label(row[k_nl]),
        en='')


def purify(row):
    k_fr = 'Purifie l\'air'
    k_nl = 'Zuivert de lucht'
    return create_quality(
        get_quality_score(row[k_fr]),
        get_quality_label(row[k_fr]),
        get_quality_label(row[k_nl]),
        en='')


def filtery(row):
    k_fr = 'Filtre l\'eau'
    k_nl = 'Filtert het water'
    return create_quality(
        get_quality_score(row[k_fr]),
        get_quality_label(row[k_fr]),
        get_quality_label(row[k_nl]),
        en='')


def floody(row):
    k_fr = 'Evite les inondations'
    k_nl = 'Voorkomt overstromingen'
    return create_quality(
        get_quality_score(row[k_fr]),
        get_quality_label(row[k_fr]),
        get_quality_label(row[k_nl]),
        en='')


def carbony(row):
    k_fr = 'Stocke le carbone'
    k_nl = 'Slaat koolstof op'
    return create_quality(
        get_quality_score(row[k_fr]),
        get_quality_label(row[k_fr]),
        get_quality_label(row[k_nl]),
        en='')


def sweety(row):
    k_fr = 'Adoucit le climat'
    k_nl = 'Verzacht het klimaat'
    return create_quality(
        get_quality_score(row[k_fr]),
        get_quality_label(row[k_fr]),
        get_quality_label(row[k_nl]),
        en='')


def soily(row):
    k_fr = 'Limite l\'érosion du sol'
    k_nl = 'Beperkt de erosie van de bodem'
    return create_quality(
        get_quality_score(row[k_fr]),
        get_quality_label(row[k_fr]),
        get_quality_label(row[k_nl]),
        en='')


def healthy(row):
    k_fr = 'Fait du bien, est utile'
    k_nl = 'Doet goed, is nuttig'
    return create_quality(
        get_quality_score(row[k_fr]),
        get_quality_label(row[k_fr]),
        get_quality_label(row[k_nl]),
        en='')


class Command(BaseCommand):
    help = """Import bota
"""

    output_transaction = True
    dones = dict()

    def add_arguments(self, parser):
        parser.add_argument('root')

    def handle(self, *args, **options):
        Taxon.objects.all().delete()
        TaxonPicture.objects.all().delete()

        self.root = Path(options['root'])
        db_path = self.root.joinpath('table.csv')
        graph_path = self.root.joinpath('graph.csv')
        self.G = nx.Graph()

        with open(graph_path.as_posix()) as f:
            for row in DictReader(f):
                self.G.add_edge(
                    row['SUJET'], obj(row['OBJET']), p=row['PREDICAT'].strip())

        with open(db_path.as_posix()) as f:
            for row in DictReader(f):
                self.process_row(row)

    def process_row(self, row):
        tid = latin(row)
        if tid in self.dones:
            return
        tx = Taxon.objects.create(
            family=family(row),
            genus=genus(row),
            species=species(row),
            trivia=trivia(row),
            name=name(row),
            origin=origin(row),
            soil=soil(row),
            climate=climate(row),
            max_age=max_age(row),
            max_height=max_height(row),
            max_circumference=max_circumference(row),
            beauty=beauty(row),
            biodiversity=biodiversity(row),
            oxygeny=oxygeny(row),
            purify=purify(row),
            filtery=filtery(row),
            floody=floody(row),
            carbony=carbony(row),
            sweety=sweety(row),
            soily=soily(row),
            healthy=healthy(row),
        )

        get_planche_bota(self.G, tx, tid)

        self.stdout.write('Created ' + self.style.SUCCESS(tid))
        self.dones[tid] = True