from django.shortcuts import render, get_object_or_404, redirect
from django.http import JsonResponse
# from django.core.serializers import serialize
from shapely.geometry import Point
from django.urls import reverse
from pyproj import Proj, transform

from .models.subject import Tree, TreePicture
from .models.ancillary import Portrait
from wood_botanique.models.species import TaxonPicture

wgs = Proj(init='epsg:4326')
lambert72 = Proj(init='epsg:31370')


def front_page(request, lang):
    """Serve the landing page
    """
    template_name = 'wood_core/site-{}/index.html'.format(lang)
    context = {
        'lang': lang,
        'counter': Tree.objects.count(),
        'monthly': Portrait.objects.filter(monthly=True).first()
    }
    return render(request, template_name, context)


def about_page(request, lang):
    """Serve the landing page
    """
    template_name = 'wood_core/site-{}/about.html'.format(lang)
    context = {}
    return render(request, template_name, context)


def atlas_map(request, lang):
    """Serve the atlas map
    """
    template_name = 'wood_core/site-{}/map.html'.format(lang)
    context = {'lang': lang}
    return render(request, template_name, context)


class GeoAcc:
    features = []

    def push(self, coords, props):
        x, y = transform(wgs, lambert72, coords[0], coords[1])
        # print('> {} {}'.format(coords[0], coords[1]))
        # print('= {} {}'.format(x, y))
        p = Point([x, y]).buffer(10)
        self.features.append(
            dict(
                type='Feature',
                geometry=dict(
                    type="Polygon", coordinates=[list(p.exterior.coords)]),
                properties=props))

    def geojson(self):
        return dict(type="FeatureCollection", features=self.features)


def atlas_data(request):
    """Serve the atlas data
    """
    geo_acc = GeoAcc()
    for tree in Tree.objects.all():
        props = dict(
            category=tree.category,
            has_portrait=tree.portrait_set.count() > 0,
            nom_fr=tree.name.fr,
            nom_nl=tree.name.nl,
            famille_fr=tree.taxon.trivia.fr,
            famille_nl=tree.taxon.trivia.nl,
            adresse_1=tree.street_address.fr,
            adres_1=tree.street_address.nl,
            latin='{} {}'.format(tree.taxon.genus, tree.taxon.species),
            wood_file='atlas/{}.html'.format(tree.id),
        )
        geo_acc.push(tree.position.coords, props)

    return JsonResponse(geo_acc.geojson())


def atlas_page(request, lang, pid):
    """Serve an identification page for a tree
    """
    template_name = 'wood_core/fiche.html'
    obj = get_object_or_404(Tree, pk=pid)
    if 'rmqe' == obj.category:
        template_name = 'wood_core/fiche-rmq.html'
    context = {
        'tree': obj,
        'lang': lang,
        'media_sizes': (2000, 1800, 1600, 1400, 1200, 1000, 800, 600, 400),
    }
    return render(
        request,
        template_name,
        context,
    )


def atlas_pdf(request):
    """Serve an identification document (PDF) for a tree
    """
    pass


def get_size(w, h, target):
    if w > h:
        r = target / w
    else:
        r = target / h
    tw = w * r
    th = h * r
    return (tw, th)


def media(request, model, id, target):
    if 'treepicture' == model:
        obj = get_object_or_404(TreePicture, pk=id)
        width, height = get_size(obj.file.width, obj.file.height, target)
        thumb = obj.file.get_thumbnail({'size': [width, height]})
        return redirect(thumb.url, permanent=True)
    elif 'tree' == model:
        obj = get_object_or_404(Tree, pk=id)
        width, height = get_size(obj.photomaton.width, obj.photomaton.height,
                                 target)
        thumb = obj.photomaton.get_thumbnail({'size': [width, height]})
        return redirect(thumb.url, permanent=True)
    elif 'taxon' == model:
        obj = get_object_or_404(TaxonPicture, pk=id)
        width, height = get_size(obj.file.width, obj.file.height, target)
        thumb = obj.file.get_thumbnail({'size': [width, height]})
        return redirect(thumb.url, permanent=True)
