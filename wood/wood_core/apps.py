from django.apps import AppConfig


class WoodCoreConfig(AppConfig):
    name = 'wood_core'
