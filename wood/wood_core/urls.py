from django.urls import path, include

from .views import (
    atlas_data,
    atlas_page,
    atlas_map,
    media,
    about_page,
    front_page,
)

urlpatterns = [
    path('media/<model>/<int:id>/<int:target>', media, name='wood-media'),
    path('atlas/data/', atlas_data, name='wood-data'),
    path('<lang>/atlas.html', atlas_map, name='wood-atlas'),
    path('<lang>/atlas/<int:pid>.html', atlas_page, name='wood-atlas-tree'),
    path('<lang>/about.html', about_page, name='wood-about'),
    path('<lang>/index.html', front_page, name='wood-index'),
    path('<lang>/', front_page),
]
