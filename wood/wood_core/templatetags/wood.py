from functools import partial
from django import template
from django.utils.safestring import mark_safe
from markdown import markdown
from wood_core.models.subject import Tree

register = template.Library()


@register.filter
def quality_score(quality):
    q = quality.quantity
    if q > 0:
        return '+' * q
    elif q < 0:
        return '-' * abs(q)

    return ''


@register.filter
def tr(message, lang):
    return getattr(message, lang, 'XXXXXXXX')


@register.filter
def anti_lang(lang):
    if 'fr' == lang:
        return 'nl'
    return 'fr'


@register.simple_tag
def tr_label(lang, **kwargs):
    return kwargs.get(lang, 'XXXXXXXX')


@register.filter
def jumelage(tree, scope):
    return tree.jumelage_set.filter(scope=scope)


@register.filter
def embed(tree, provider):
    return tree.embedmedia_set.filter(provider=provider)


@register.filter
def md(content):
    return mark_safe(markdown(content))


def get_size(w, h, target):
    if w > h:
        r = target / w
    else:
        r = target / h
    tw = w * r
    th = h * r
    return (tw, th)


@register.filter
def width(image, target):
    return get_size(image.width, image.height, target)[0]


@register.filter
def height(image, target):
    return get_size(image.width, image.height, target)[1]


@register.filter
def ratio(image, target):
    if image.width is None:
        return ''
    w, h = get_size(image.width, image.height, target)
    if h / w * 100 < 75:
        return h / w * 100
    else:
        return 75


# MEDIA_CSS_TPL = """
# @media (max-width: {media_width}px) {{
#   #{media_id} {{
#     background-image: url("{image_path}");
#     padding-top: {image_ratio}%;
#   }}
# }}
# """

# @register.filter
# def media_css(pic, id):
#     if pic.width is None:
#         return ''
#     sizes = (2000, 1800, 1600, 1400, 1200, 1000, 800, 600, 400)
#     sizer = partial(get_size, pic.width, pic.height)
#     css = []
#     for sz, (w, h) in zip(sizes, map(sizer, sizes)):
#         if h / w * 100 < 75:
#             ratio = h / w * 100
#         else:
#             ratio = 75

#         thumb = pic.get_thumbnail({'size': [w, h]})
#         row_css = MEDIA_CSS_TPL.format(
#             media_width=sz,
#             media_id='id_{}'.format(id),
#             image_path=thumb.url,
#             image_ratio=ratio,
#         )
#         css.append(row_css)
#     return mark_safe('\n'.join(css))


@register.filter
def imported(id):
    try:
        return Tree.objects.get(import_id='ID {}'.format(id)).id
    except Exception:
        return id