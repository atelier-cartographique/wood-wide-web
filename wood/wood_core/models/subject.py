#  Copyright (C) 2018 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from django.contrib.gis.db import models
from easy_thumbnails.fields import ThumbnailerImageField

from wood_lingua.models import message_field
from wood_botanique.models.species import Taxon

from .media import picture, slugify
from .ancillary import BaseLink


class TreeManager(models.Manager):
    def get_queryset(self):
        select = (
            'name',
            'description',
            'street_address',
            'city',
            'post_code',
        )
        return super().get_queryset().select_related(*select)


def upload_to_photomaton(instance, fn):
    id = instance.id
    return 'phototomaton/{}/{}'.format(id, slugify(fn))


class Tree(models.Model):
    objects = TreeManager()

    REMARQUABLE = 'rmqb'
    REMARQUE = 'rmqe'
    ADOPTE = 'adpt'
    CATEGORY = (
        (REMARQUABLE, 'Remarquable'),
        (REMARQUE, 'Remarqué'),
        (ADOPTE, 'Adopté'),
    )

    id = models.AutoField(primary_key=True)
    patrimoine = models.IntegerField(blank=True, null=True)
    import_id = models.CharField(max_length=12)
    category = models.CharField(max_length=4, choices=CATEGORY)
    taxon = models.ForeignKey(Taxon, on_delete=models.CASCADE)

    photomaton = ThumbnailerImageField(upload_to=upload_to_photomaton)

    name = message_field('tree_name')
    description = message_field('tree_description')

    street_address = message_field('tree_street_address')
    city = message_field('tree_city')
    post_code = message_field('tree_post_code')
    position = models.PointField(srid=4326)

    height = message_field('tree_height')
    circumference = message_field('tree_circumference')
    crown_diameter = message_field('tree_crown_diameter')

    def __str__(self):
        return '{} ({})'.format(self.name, self.id)


TreePicture = picture(Tree, is_sorted=True)


class Remarqueur(BaseLink):
    url = message_field('remarqueur_url')
    label = message_field('remarqueur_label')


class Activity(BaseLink):
    url = message_field('activity_url')
    label = message_field('activity_label')


class LinkedMedia(BaseLink):
    url = models.URLField()
    label = message_field('linked_media_label')


class Jumelage(BaseLink):
    BELGIQUE = 'belgique'
    EUROPE = 'europe'
    MONDE = 'monde'
    SCOPE = (
        (BELGIQUE, 'Belgique'),
        (EUROPE, 'Europe'),
        (MONDE, 'Monde'),
    )
    scope = models.CharField(max_length=12, choices=SCOPE)

    url = models.URLField()
    label = message_field('jumelage_label')