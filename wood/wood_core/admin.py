from django.contrib.gis.admin import OSMGeoAdmin as GisModelAdmin
from django.contrib.admin import site, ModelAdmin, SimpleListFilter, StackedInline
from easy_thumbnails.widgets import ImageClearableFileInput
from easy_thumbnails.fields import ThumbnailerImageField

from .models.subject import Tree, TreePicture, Jumelage, LinkedMedia, Remarqueur, Activity
from .models.ancillary import Portrait
from .models.media import EmbedMedia


class TreePictureWidget(ImageClearableFileInput):
    def render(self, name, value, attrs=None, renderer=None):
        return super().render(name, value, attrs)


class TreePictureInline(StackedInline):
    formfield_overrides = {
        ThumbnailerImageField: dict(widget=TreePictureWidget)
    }
    model = TreePicture
    autocomplete_fields = search_fields = ('caption', )


class TreeAdmin(GisModelAdmin):
    list_display = ('name', 'taxon', 'city', 'street_address')
    list_filter = (
        'category',
        'city__fr',
    )
    inlines = (TreePictureInline, )

    autocomplete_fields = (
        'height',
        'circumference',
        'crown_diameter',
        'name',
        'taxon',
        'description',
        'street_address',
        'city',
        'post_code',
    )

    search_fields = (
        'name__fr',
        'name__nl',
    )


site.register(Tree, TreeAdmin)


def image_name(instance):
    return instance.file.name


class PictureAdmin(ModelAdmin):
    list_display = (image_name, 'linked_object', 'caption', 'sort_value')
    autocomplete_fields = search_fields = ('caption', )


class PortraitAdmin(ModelAdmin):
    list_display = ('tree', 'monthly')


site.register(TreePicture, PictureAdmin)

site.register(Jumelage)
site.register(LinkedMedia)
site.register(Remarqueur)
site.register(Activity)
site.register(Portrait, PortraitAdmin)
site.register(EmbedMedia)