from django.db import models
from django.forms import ModelForm, HiddenInput, CharField
from django.contrib.admin import site, ModelAdmin

from ..apps import WoodLinguaConfig
from .record import MessageRecord, MessageManager

app_name = WoodLinguaConfig.name

models_collection = dict()


class MessageAdmin(ModelAdmin):

    list_display = ('fr', 'nl', 'en')
    search_fields = ('fr', 'nl', 'en')


def register_admin(message_type):
    mod = models_collection[message_type]
    # frm = type(
    #     'Form_{}'.format(message_type), (ModelForm, ),
    #     dict(t=CharField(widget=HiddenInput(), initial=message_type)))
    adm = type(
        'Admin_{}'.format(message_type),
        (MessageAdmin, ),
        dict(search_fields=['fr', 'nl', 'en']),
    )
    site.register(mod, adm)


def get_model(message_type):
    if message_type not in models_collection:
        meta = type('Meta', (), dict(app_label=app_name))
        M = type(
            message_type,
            (MessageRecord, ),
            dict(
                __module__='{}.models'.format(app_name),
                Meta=meta,
                objects=(MessageManager())),
        )

        models_collection[message_type] = M
        # site.register(M, MessageAdmin)
        register_admin(message_type)

    return models_collection[message_type]


def message_field(message_type):
    return models.ForeignKey(
        get_model(message_type),
        on_delete=models.CASCADE,
        related_name=message_type,
    )


def message(message_type, fr=None, nl=None, en=None):
    mod = get_model(message_type)
    return mod.objects.create(fr=fr, nl=nl, en=en)


def simple_message(t, msg):
    return message(message_type=t, fr=msg, nl=msg, en=msg)