#  Copyright (C) 2018 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from django.db import models
from django.conf import settings

LINGUA_LANGUAGE = getattr(settings, 'LINGUA_LANGUAGE', 'en')


class MessageManager(models.Manager):
    pass
    # def __init__(self, *args, **kwargs):
    #     self.message_type = kwargs.pop('message_type')
    #     super().__init__(*args, **kwargs)

    # def get_queryset(self):
    #     return super().get_queryset().filter(t=self.message_type)


class MessageRecord(models.Model):
    class Meta:
        abstract = True

    # t = models.CharField('Message Type', max_length=32)
    fr = models.TextField(blank=True)
    nl = models.TextField(blank=True)
    en = models.TextField(blank=True)

    def __str__(self):
        label = getattr(self, LINGUA_LANGUAGE,
                        'LINGUA_LANGUAGE should be one of en, fr, nl.')
        end = ''
        if len(label) > 64:
            end = '…'
        return '{}{}'.format(label[:64], end)

    def update_record(self, fr='', nl='', en='', parameters=None):
        self.fr = fr
        self.nl = nl
        self.en = en
        self.save()

    def to_dict(self):
        return dict(fr=self.fr, nl=self.nl, en=self.en)
