package main

import (
	"encoding/base64"
)

func base64decodeSql(input string) string {
    content, err := base64.StdEncoding.DecodeString(input)
    if err != nil {
        return input
    }
    return string(content)
}



const QueryCreateTableForms = "CreateTableForms"

const QueryCreateTableRecords = "CreateTableRecords"

const QueryInsertForm = "InsertForm"

const QueryInsertRecord = "InsertRecord"

const QuerySelectForm = "SelectForm"

const QuerySelectFormAll = "SelectFormAll"

const QuerySelectFormExists = "SelectFormExists"

const QuerySelectFormRecords = "SelectFormRecords"

const QuerySelectTableExists = "SelectTableExists"


func RegisterQueries(store Store) {
	
	store.Register(QueryCreateTableForms, base64decodeSql(`Q1JFQVRFIFRBQkxFIHt7IC5Gb3JtcyB9fSAoCiAgICAiaWQiIFRFWFQgUFJJTUFSWSBLRVksCiAgICAibmFtZSIgVEVYVCwKICAgICJsYW5nIiBURVhULAogICAgInN0YXR1cyIgVEVYVAopOw==`))
	
	store.Register(QueryCreateTableRecords, base64decodeSql(`Q1JFQVRFIFRBQkxFIHt7IC5SZWNvcmRzIH19ICgKICAgICJpZCIgVEVYVCBQUklNQVJZIEtFWSwKICAgICJmaWQiIFRFWFQsCiAgICAia2V5IiBURVhULAogICAgInZhbHVlIiBURVhULAogICAgInRzIiBEQVRFVElNRSBERUZBVUxUIENVUlJFTlRfVElNRVNUQU1QCik7`))
	
	store.Register(QueryInsertForm, base64decodeSql(`SU5TRVJUIElOVE8ge3sgLkZvcm1zIH19CigKICAgICJpZCIsCiAgICAibmFtZSIsCiAgICAibGFuZyIsCiAgICAic3RhdHVzIgopClZBTFVFUygkMSwgJDIsICQzLCAkNCk7`))
	
	store.Register(QueryInsertRecord, base64decodeSql(`SU5TRVJUIElOVE8ge3sgLlJlY29yZHMgfX0KKAogICAgImlkIiwKICAgICJmaWQiLAogICAgImtleSIsCiAgICAidmFsdWUiCikKVkFMVUVTKCQxLCAkMiwgJDMsICQ0KTs=`))
	
	store.Register(QuerySelectForm, base64decodeSql(`U0VMRUNUICoKRlJPTSAKICAgIHt7IC5Gb3JtcyB9fSBmCldIRVJFIGYuaWQgPSAkMTsK`))
	
	store.Register(QuerySelectFormAll, base64decodeSql(`U0VMRUNUICoKRlJPTSAKICAgIHt7IC5Gb3JtcyB9fTsK`))
	
	store.Register(QuerySelectFormExists, base64decodeSql(`LS0gIGh0dHBzOi8vc3RhY2tvdmVyZmxvdy5jb20vcXVlc3Rpb25zLzEwMzc3NzgxL3JldHVybi1ib29sZWFuLXZhbHVlLW9uLXNxbC1zZWxlY3Qtc3RhdGVtZW50ClNFTEVDVCAKICAgIENBU1QoCiAgICAgICAgRVhJU1RTKFNFTEVDVCAqIEZST00ge3sgLkZvcm1zIH19IFdIRVJFIGlkID0gJDEpIAogICAgICAgIEFTIEJJVAogICAgICAgICk7`))
	
	store.Register(QuerySelectFormRecords, base64decodeSql(`U0VMRUNUIGYubmFtZSwgZi5sYW5nLCBmLnN0YXR1cywgci5pZCwgci5rZXksIHIudmFsdWUsIHIudHMKRlJPTSAKICAgIHt7IC5Gb3JtcyB9fSBmCiAgICBJTk5FUiBKT0lOIHt7IC5SZWNvcmRzIH19IHIgT04gZi5pZCA9IHIuZmlkCldIRVJFIGYuaWQgPSAkMTsK`))
	
	store.Register(QuerySelectTableExists, base64decodeSql(`U0VMRUNUIG5hbWUgCiAgICBGUk9NIHNxbGl0ZV9tYXN0ZXIgCiAgICBXSEVSRSB0eXBlPSd0YWJsZScgQU5EIG5hbWU9JDEgOwogICAg`))
	
}