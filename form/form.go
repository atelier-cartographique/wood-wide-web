/*
 *  Copyright (C) 2018 Pierre Marchand <pierre.m@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
//go:generate futil -type func String=string Int=int Bool=bool
//go:generate futil -type option -import time  String=string UInt64=uint64 Node=Node Time=time.Time  Int=int UInt=uint
//go:generate futil -type result Bool=bool Node=Node  String=string Int=int   Int64=int64 ByteSlice=[]byte Document=Document  Error=error InfoUnitRecord=InfoUnitRecord InfoUnit=InfoUnit InfoRecord=InfoRecord UnitStore=UnitStore
//go:generate futil -type array   Int=int String=string Node=Node InfoUnitRecord=InfoUnitRecord InfoUnit=InfoUnit InfoRecord=InfoRecord UnitStore=UnitStore
//go:generate webgen -output style.go -what css
//go:generate webgen -output js.go -what js
//go:generate webgen -output sql.go -what sql -prefix=Query
package main

import (
	"flag"
	"log"

	"github.com/satori/go.uuid"
)

var (
	forms        string
	records      string
	admin        string
	httpdI       string
	tableRecords string
	tableForms   string
)

func init() {
	flag.StringVar(&admin, "admin", uuid.Must(uuid.NewV4()).String(), "A token to protect admin site")
	flag.StringVar(&forms, "decl", "forms/", "Folder path where to find form declarations")
	flag.StringVar(&records, "output", "output.db", "Sqlite db file")
	flag.StringVar(&httpdI, "http", "0.0.0.0:8080", "interface for httpd")
	flag.StringVar(&tableRecords, "records", "records", "Records table")
	flag.StringVar(&tableForms, "forms", "forms", "Forms table")
}

func controller(cont chan string) {
	for {
		rec := <-cont
		log.Println(rec)
	}
}

func checktables(store Store) {
	var (
		name string
		rt   = true
		ft   = true
	)
	store.QueryFunc(QuerySelectTableExists, store.Tables.Records)(RowCallback(func() {
		rt = false
	}), &name)

	store.QueryFunc(QuerySelectTableExists, store.Tables.Forms)(RowCallback(func() {
		ft = false
	}), &name)

	if rt {
		log.Printf("Create(%s) %s", QueryCreateTableRecords, store.Tables.Records)
		store.QueryFunc(QueryCreateTableRecords).Exec()
	}

	if ft {
		log.Printf("Create(%s) %s", QueryCreateTableForms, store.Tables.Forms)
		store.QueryFunc(QueryCreateTableForms).Exec()
	}
}

func main() {
	flag.Parse()

	store := NewStore(records, Tables{tableRecords, tableForms})

	RegisterQueries(store)
	checktables(store)

	cont := make(chan string)
	ctx := Context{cont, httpdI, store, GetInfoRecords(forms), admin}
	go StartHTTP(ctx)
	log.Printf("Admin Site Token: %s", admin)
	controller(cont)
}
