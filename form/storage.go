/*
 *  Copyright (C) 2018 Pierre Marchand <pierre.m@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package main

import (
	"errors"
	"fmt"
	"log"
	"sync"

	"strings"
	"text/template"

	"database/sql"

	_ "github.com/mattn/go-sqlite3"
)

type Tables struct {
	Records string
	Forms   string
}

type Querier func(args ...interface{}) (*sql.Rows, error)

var namedQueriesMut sync.Mutex
var queryMut sync.Mutex
var namedQueries = map[string]string{}

var cachedQueriers = map[string]Querier{}

func noopQ(name string, err string) Querier {
	return func(args ...interface{}) (*sql.Rows, error) {
		return nil, errors.New(fmt.Sprintf("Noop(%s): %s", name, err))
	}
}

func (c Tables) makeQ(db *sql.DB, name string) Querier {
	qs, ok := namedQueries[name]
	log.Printf("Tables.makeQ %s %v", name, ok)
	if ok == false {
		return noopQ(name, "Not in namedQueries table")
	}

	var builder strings.Builder
	queryTemplate := template.New(name)
	parsed, err := queryTemplate.Parse(qs)
	if err != nil {
		return noopQ(name, err.Error())
	}

	err = parsed.Execute(&builder, c)
	if err != nil {
		return noopQ(name, err.Error())
	}

	qs = builder.String()

	return func(args ...interface{}) (*sql.Rows, error) {
		queryMut.Lock()
		defer func() {
			log.Println("Unlocking DB queries")
			queryMut.Unlock()
		}()

		return db.Query(qs, args...)
	}
}

func (c Tables) q(db *sql.DB, name string) Querier {
	f, ok := cachedQueriers[name]
	if ok == false {
		f = c.makeQ(db, name)
		cachedQueriers[name] = f
	}

	return f
}

type Store struct {
	Tables Tables
	db     *sql.DB
}

func (store Store) Query(name string, args ...interface{}) (*sql.Rows, error) {
	f := store.Tables.q(store.db, name)
	return f(args...)
}

func (store Store) Register(name string, qs string) {
	log.Printf("Store.Register %s", name)
	namedQueriesMut.Lock()
	defer namedQueriesMut.Unlock()
	namedQueries[name] = qs
}

type queryFunc func(cb rowsCb, cells ...interface{}) ResultBool

func (qf queryFunc) Exec(cells ...interface{}) ResultBool {
	return qf(RowCallback(func() {}), cells...)
}

func (store Store) QueryFunc(name string, args ...interface{}) queryFunc {
	f := func(cb rowsCb, cells ...interface{}) ResultBool {
		rows, err := store.Query(name, args...)
		if err != nil {
			log.Printf("Query Error(%s): %s", name, err.Error())
			return ErrBool(err)
		}
		defer func() {
			rows.Close()
		}()
		WithRows(rows, cb, cells...)
		return OkBool(true)
	}

	return f
}

func NewStore(path string, tables Tables) Store {
	db, err := sql.Open("sqlite3", path)
	if err != nil {
		log.Fatalf("Could not open database: %s", err)
	}

	return Store{tables, db}

}

type rowsCb func(args ...interface{})

func WithRows(rows *sql.Rows, cb rowsCb, args ...interface{}) {
	defer rows.Close()
	for rows.Next() {
		scanError := rows.Scan(args...)
		if scanError != nil {
			log.Printf("Error [WithRows] %s", scanError.Error())
		} else {
			cb(args...)
		}
	}
	rows.Close()
}

func RowCallback(f func()) rowsCb {
	return func(args ...interface{}) {
		f()
	}
}
