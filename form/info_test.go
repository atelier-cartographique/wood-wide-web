package main

import (
	"reflect"
	"testing"
)

func TestGetInfoRecords(t *testing.T) {
	type args struct {
		dn string
	}

	want := ArrayInfoRecordFrom([]InfoRecord{
		InfoRecord{
			"form0",
			"fr",
			"Un Test",
			"Description du test",
			[]InfoUnitRecord{
				InfoUnitRecord{"unit", "unité d'information", "Des explications", "text"},
				InfoUnitRecord{"unit2", "unité d'information 2", "Des explications 2", "number"},
			},
		},
	}...)

	tests := []struct {
		name string
		args args
		want ArrayInfoRecord
	}{
		{
			"load info records",
			args{"test_data/records/"},
			want,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetInfoRecords(tt.args.dn); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetInfoRecords() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewInfo(t *testing.T) {
	type args struct {
		dn string
	}

	want := ""

	tests := []struct {
		name string
		args args
		want string
	}{
		{
			"make form",
			args{"test_data/records/"},
			want,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			GetInfoRecords(tt.args.dn).
				First().
				FoldF(
					func(e error) {
						t.Error(e)
					},
					func(rec InfoRecord) {
						info := NewInfo("test", rec)
						if got := info.MakeNode().Render(); !reflect.DeepEqual(got, tt.want) {
							t.Errorf("NewInfo() = %v, want %v", got, tt.want)
						}
					})
		})
	}
}
