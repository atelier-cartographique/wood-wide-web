"use strict";
(function () {
    const start = () => {
        const reg = [];
        const submit = document.querySelector("#submit");
        if (submit != null) {
            const formId = submit.getAttribute('data-ref');
            const str = JSON.stringify;
            submit.addEventListener('click', () => {
                const url = `/record/${formId}`;
                const headers = new Headers();
                const method = 'POST';
                headers.append('Content-Type', 'application/json');
                Promise.all(reg.map(({ key, val }) => fetch(url, { method, body: str({ formId, key, value: val() }), headers })))
                    .then((resp) => {
                    // window.location.href = `/`
                    console.log(`Success ${resp.length}`);
                })
                    .catch(err => console.error(err));
            });
        }
        const registerUnit = (node) => {
            const key = node.getAttribute('data-name') || '__missing_key__';
            const input = node.querySelector('input');
            if (input) {
                reg.push({
                    key, val: () => input.value
                });
            }
        };
        Array.from(document.querySelectorAll('.form-unit')).forEach(registerUnit);
    };
    document.addEventListener('DOMContentLoaded', start);
})();
//# sourceMappingURL=app.js.map