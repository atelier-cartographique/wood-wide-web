--  https://stackoverflow.com/questions/10377781/return-boolean-value-on-sql-select-statement
SELECT 
    CAST(
        EXISTS(SELECT * FROM {{ .Forms }} WHERE id = $1) 
        AS BIT
        );