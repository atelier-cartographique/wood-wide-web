CREATE TABLE {{ .Records }} (
    "id" TEXT PRIMARY KEY,
    "fid" TEXT,
    "key" TEXT,
    "value" TEXT,
    "ts" DATETIME DEFAULT CURRENT_TIMESTAMP
);