SELECT f.name, f.lang, f.status, r.id, r.key, r.value, r.ts
FROM 
    {{ .Forms }} f
    INNER JOIN {{ .Records }} r ON f.id = r.fid
WHERE f.id = $1;
