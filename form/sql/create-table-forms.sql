CREATE TABLE {{ .Forms }} (
    "id" TEXT PRIMARY KEY,
    "name" TEXT,
    "lang" TEXT,
    "status" TEXT
);