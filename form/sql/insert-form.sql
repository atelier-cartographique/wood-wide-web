INSERT INTO {{ .Forms }}
(
    "id",
    "name",
    "lang",
    "status"
)
VALUES($1, $2, $3, $4);