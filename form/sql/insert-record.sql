INSERT INTO {{ .Records }}
(
    "id",
    "fid",
    "key",
    "value"
)
VALUES($1, $2, $3, $4);