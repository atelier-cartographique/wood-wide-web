package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
)

const (
	InfoKindString = "text"
	InfoKindNumber = "number"
)

const (
	InfoStatusNew     = "new"
	InfoStatusFilled  = "fil"
	InfoStatusChecked = "chk"
)

type InfoUnit struct {
	name   string
	lang   string
	label  string
	legend string
	kind   string
}

func (iu InfoUnit) Name() string {
	return iu.name
}

func (iu InfoUnit) Lang() string {
	return iu.lang
}

func (iu InfoUnit) Label() string {
	return iu.label
}

func (iu InfoUnit) Legend() string {
	return iu.legend
}

func (iu InfoUnit) Kind() string {
	return iu.kind
}

func (iu InfoUnit) makeNode(v string, id string) Node {
	return Div(ClassAttr("form-unit collection__item").
		Set("data-name", iu.Name()).
		Set("data-lang", iu.Lang()).
		Set("data-id", id),
		Div(ClassAttr("form-legend"), Text(iu.Legend())),
		Div(ClassAttr("form-label"), Text(iu.Label())),
		Div(ClassAttr("form-input input-wrapper"),
			Input(NewAttr().Set("type", iu.Kind()).Set("value", v))))
}

func (iu InfoUnit) MakeNode() Node {
	return iu.makeNode("", "__NO_ID__")
}

func (iu InfoUnit) MakeNodeWithStore(a ArrayUnitStore) Node {
	return a.Find(func(u UnitStore) bool { return iu.Name() == u.key }).FoldNodeF(
		func(_ error) Node {
			return iu.MakeNode()
		},
		func(u UnitStore) Node {
			return iu.makeNode(u.value, u.rid)
		})
}

type Info struct {
	ID          string
	Name        string
	Lang        string
	Title       string
	Description string
	units       ArrayInfoUnit
}

func (i Info) GetUnit(idx int) ResultInfoUnit {
	return i.units.At(idx)
}

func (i Info) makeNode(a ArrayUnitStore) Node {

	units := i.units.MapNode(func(u InfoUnit) Node { return u.MakeNodeWithStore(a) })
	info := Div(ClassAttr("form-info collection"),
		H1(NewAttr(), Text(i.Title)),
		P(ClassAttr("form-description"), Text(i.Description)))
	info.Append(units...)
	info.Append(Div(ClassAttr("submit-block"),
		Div(ClassAttr("btn btn--submit").Set("id", "submit").Set("data-ref", i.ID), Text("Save"))))

	return info
}

func (i Info) MakeNode() Node {
	sunits := 
	return i.makeNode(ArrayInfoUnitFrom(make([]InfoUnit, 0)))
}

type InfoUnitRecord struct {
	Name   string `json:"name"`
	Label  string `json:"label"`
	Legend string `json:"legend"`
	Kind   string `json:"type"`
}

type InfoRecord struct {
	Name        string           `json:"name"`
	Lang        string           `json:"lang"`
	Title       string           `json:"title"`
	Description string           `json:"description"`
	Units       []InfoUnitRecord `json:"units"`
}

type AnswerRecord struct {
	FormID string `json:"formId"`
	Key    string `json:"key"`
	Value  string `json:"value"`
}

func NewInfo(id string, rec InfoRecord) Info {
	unitIndex := 0
	units := ArrayInfoUnitRecordFrom(rec.Units...).MapInfoUnit(func(r InfoUnitRecord) InfoUnit {
		u := InfoUnit{
			r.Name,
			rec.Lang,
			r.Label,
			r.Legend,
			r.Kind,
		}
		unitIndex++
		return u
	})

	return Info{id, rec.Name, rec.Lang, rec.Title, rec.Description, units}
}

type UnitStore struct {
	rid   string
	key   string
	value string
}

func MakeNodeFromStore(rec InfoRecord, id string, store Store) Node {

	info := NewInfo(id, rec)

	storedunits := make([]UnitStore, 0)

	var (
		name   string
		lang   string
		status string
		rid    string
		key    string
		value  string
	)

	qf := store.QueryFunc(QuerySelectFormRecords, id)
	qf(RowCallback(func() {
		storedunits = append(storedunits, UnitStore{rid, key, value})
	}), &name, &lang, &status, &rid, &key, &value)

	return info.makeNode(ArrayUnitStoreFrom(storedunits...))
}

func GetInfoRecords(dn string) ArrayInfoRecord {
	pat := fmt.Sprintf("%s/*.json", filepath.Clean(dn))
	fns, err := filepath.Glob(pat)
	if err != nil {
		return ArrayInfoRecordFrom()
	}

	return ArrayStringFrom(fns...).MapInfoRecord(func(fn string) InfoRecord {
		file, err := os.Open(fn)
		if err != nil {
			log.Fatalf("Could not Open %v, %v", fn, err)
		}
		c, _ := ioutil.ReadAll(file)
		defer file.Close()
		t := InfoRecord{}
		err = json.Unmarshal(c, &t)
		if err != nil {
			log.Fatalf("Could not decode %v, %v", fn, err)
		}
		return t
	})
}
