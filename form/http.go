/*
 *  Copyright (C) 2018 Pierre Marchand <pierre.m@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/labstack/echo"
	"github.com/satori/go.uuid"
)

func makeDocument(page string, hn ...Node) Document {
	doc := NewDoc(NewAttr().Set("data-page", page))
	doc.head.Append(HeadMeta(NewAttr().Set("charset", "utf-8")))
	doc.head.Append(HeadMeta(NewAttr().
		Set("name", "viewport").
		Set("content", "width=device-width, initial-scale=1.0")))
	doc.head.Append(Style(NewAttr(), Text(CssOl)))
	doc.head.Append(HeadLink(NewAttr().
		Set("rel", "stylesheet").
		Set("href", "https://atelier-cartographique.be/css-tower/css/framework.css")))
	doc.head.Append(Style(NewAttr(), Text(CssStyle)))
	doc.head.Append(Script(NewAttr(), Text(JsProj4)))
	doc.head.Append(Script(NewAttr(), Text(JsOl)))
	doc.head.Append(Script(NewAttr(), Text(JsApp)))
	for _, n := range hn {
		doc.head.Append(n)
	}
	return doc
}

func indexHandler(app *echo.Echo, context Context) {

	forms := context.InfoRecords.MapNode(func(rec InfoRecord) Node {
		return Div(ClassAttr("form-link"),
			A(NewAttr().Set("href", fmt.Sprintf("/form/%s", rec.Name)),
				Text(rec.Title)))
	}).Slice()

	app.GET("/", func(c echo.Context) error {
		doc := NewDoc(NewAttr().Set("data-page", "index"))
		doc.head.Append(HeadMeta(NewAttr().Set("charset", "utf-8")))
		doc.head.Append(HeadMeta(NewAttr().
			Set("name", "viewport").
			Set("content", "width=device-width, initial-scale=1.0")))
		doc.head.Append(Style(NewAttr(), Text(CssStyle)))
		doc.body.Append(
			H1(NewAttr(), Text("Wood Wide Web")),
			P(NewAttr(), Text("List of available forms")))
		doc.body.Append(forms...)

		return c.HTML(http.StatusOK, doc.Render())
	})

}

func tools() Node {
	return Div(ClassAttr("ss"),
		Div(ClassAttr("btn btn--submit submit"), Text("Record")),
		Div(ClassAttr("btn btn--skip skip"), Text("Skip")))
}

type infoMaker func(InfoRecord) error

func formHandler(app *echo.Echo, context Context) {
	mkInfo := func(c echo.Context) infoMaker {
		return func(rec InfoRecord) error {
			id := uuid.Must(uuid.NewV4()).String()
			qf := context.Store.QueryFunc(QueryInsertForm, id, rec.Name, rec.Lang, InfoStatusNew)
			return qf.Exec().FoldError(
				echo.NewHTTPError(http.StatusInternalServerError),
				func(_ bool) error {
					doc := makeDocument(rec.Name)
					info := NewInfo(id, rec)
					doc.body.Append(info.MakeNode())
					return c.HTML(http.StatusOK, doc.Render())
				})
		}
	}

	mkInfoUpdate := func(c echo.Context) infoMaker {
		fid := c.Param("fid")

		return func(rec InfoRecord) error {

			qf := context.Store.QueryFunc(QueryInsertForm, id, rec.Name, rec.Lang, InfoStatusNew)
			return qf.Exec().FoldError(
				echo.NewHTTPError(http.StatusInternalServerError),
				func(_ bool) error {
					doc := makeDocument(rec.Name)
					info := NewInfo(fid, rec)
					doc.body.Append(info.MakeNode())
					return c.HTML(http.StatusOK, doc.Render())
				})
		}
	}

	createHandler := func(rec InfoRecord) func(echo.Context) error {
		return func(c echo.Context) error {
			return mkInfo(c)(rec)
		}
	}

	updateHandler := func(rec InfoRecord) func(echo.Context) error {
		return func(c echo.Context) error {
			return mkInfo(c)(rec)
		}
	}

	context.InfoRecords.Each(func(rec InfoRecord) {
		app.GET(fmt.Sprintf("/form/%s", rec.Name), createHandler(rec))
		app.GET(fmt.Sprintf("/form/%s/:fid", rec.Name), updateHandler(rec))
	})
}

func recordHandler(app *echo.Echo, context Context) {

	createRecord := func(fid string, r AnswerRecord) ResultString {
		id := uuid.Must(uuid.NewV4()).String()
		qf := context.Store.QueryFunc(QueryInsertRecord, id, fid, r.Key, r.Value)
		if qf.Exec().FoldBool(false, IdBool) {
			return OkString(id)
		}
		return ErrString("Failed To Create Record")
	}

	checkForm := func(fid string) ResultBool {
		exists := false
		return context.Store.QueryFunc(QuerySelectFormExists, fid).
			Exec(&exists).
			MapBool(func(_ bool) bool { return exists })
	}

	handler := func(c echo.Context) error {
		ref := c.Param("ref")

		answer := AnswerRecord{}
		if err := c.Bind(&answer); err != nil {
			log.Printf("Failed To Bind %v", err)
			return err
		}

		if answer.FormID != ref {
			return echo.NewHTTPError(http.StatusBadRequest, "inconsistent fid and ref")
		}

		return checkForm(ref).FoldError(
			echo.NewHTTPError(http.StatusInternalServerError),
			func(exists bool) error {
				if exists {
					return createRecord(ref, answer).FoldErrorF(
						func(err error) error { return echo.NewHTTPError(http.StatusInternalServerError, err) },
						func(rid string) error { return c.JSON(http.StatusCreated, answer) })
				}
				return echo.ErrNotFound
			})
	}

	app.POST("/record/:ref", handler)
}

func adminHandler(app *echo.Echo, context Context) {
	adminPath := func(p string) string {
		path := fmt.Sprintf("/%s%s", context.AdminToken, p)
		log.Printf("Admin Path(%s): %s", p, path)
		return path
	}

	handler := func(c echo.Context) error {
		doc := makeDocument("admin/forms")
		forms := Div(ClassAttr("collection"))

		var (
			id     string
			name   string
			lang   string
			status string
		)

		context.Store.QueryFunc(QuerySelectFormAll)(RowCallback(func() {
			url := fmt.Sprintf("/form/%s/%s", name, id)
			forms.Append(
				Div(ClassAttr("collection__item"),
					H2(NewAttr(), Text(name)),
					Span(ClassAttr("detail"), Textf("status: %s, lang: %s", status, lang)),
					A(NewAttr().Set("href", url), Text(url))))
		}), &id, &name, &lang, &status)

		doc.body.Append(H1(NewAttr(), Text("Forms")), forms)
		return c.HTML(http.StatusOK, doc.Render())
	}

	app.GET(adminPath("/forms"), handler)
}

func regHTTPHandlers(app *echo.Echo, context Context) {
	indexHandler(app, context)
	formHandler(app, context)
	recordHandler(app, context)
	adminHandler(app, context)
}

func StartHTTP(context Context) {
	app := echo.New()
	regHTTPHandlers(app, context)
	app.Start(context.Iface)
}
