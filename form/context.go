package main

type Context struct {
	Com         chan string
	Iface       string
	Store       Store
	InfoRecords ArrayInfoRecord
	AdminToken  string
}
