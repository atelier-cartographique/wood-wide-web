const gulp = require('gulp');
const less = require('gulp-less');
const autoprefixer = require('gulp-autoprefixer');


gulp.task('less', function () {
    gulp.src('styles/less/style.less')
        .pipe(less())
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('styles/css'));
});


gulp.task('watch', function() {
    gulp.watch('styles/less/**/*.less', ['less']);
});
