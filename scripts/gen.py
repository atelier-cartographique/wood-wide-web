#!/usr/bin/env python3

import os
import math
from stat import *
from tempfile import TemporaryFile
from hashlib import md5
import sys
import csv
import unicodedata
import re
import logging
import json
from PIL import Image
from pathlib import Path
from pybars import Compiler
from glob import glob
from PIL.ExifTags import TAGS
from functools import partial
from collections import namedtuple
import networkx as nx
import svgwrite as svg
from markdown import markdown
from uuid import uuid4
from urllib.parse import urlparse

Image.init()  # we need this for is_image

logger = logging.getLogger('Gen')

ImageDataRow = namedtuple('ImageDataRow', ['sz', 'w', 'h', 'path'])


def ensure_parent_dir(p):
    path = Path(p)
    if path.parent.exists() is False:
        path.parent.mkdir(parents=True)


SIZES = (2000, 1800, 1600, 1400, 1200, 1000, 800, 600, 400)


def is_image(path):
    if path.is_file():
        ext = path.suffix.lower()
        if ext in Image.EXTENSION:
            mime_id = Image.EXTENSION[ext]
            if mime_id in Image.MIME:
                return True
    return False


def uniq_id(data):
    hex = md5(data).hexdigest()
    return hex


def get_size(w, h, target):
    if w > h:
        r = target / w
    else:
        r = target / h
    tw = w * r
    th = h * r
    return (tw, th)


def get_exif(tag, ex):
    for k, n in TAGS.items():
        if n == tag:
            return ex[k]


get_orientation = partial(get_exif, 'Orientation')


def img_orientation(im):
    try:
        o = get_orientation(im._getexif())
        if 3 == o:
            return im.rotate(180)
        elif 6 == o:
            return im.rotate(270, expand=True)
        elif 8 == o:
            return im.rotate(90, expand=True)
        return im
    except Exception:
        return im


class ImageError(Exception):
    pass


class NoCache(Exception):
    pass


class WebImage:
    def __init__(self, path, out_dirname):
        self.path = path
        self.out_dirname = out_dirname
        stat_info = os.stat(path.as_posix())
        self.basename = uniq_id('{}.{}'.format(
            stat_info[ST_MTIME], stat_info[ST_SIZE]).encode('utf8'))

    def get_extension(self):
        return self.path.suffix

    def get_target_path(self, sz=None, ext=None):
        if not ext:
            ext = self.get_extension()
        if sz:
            return '{}/{}_{}{}'.format(self.out_dirname, self.basename, sz,
                                       ext)
        return '{}/{}{}'.format(self.out_dirname, self.basename, ext)

    def build_data(self, im):
        data = []
        data.append(
            ImageDataRow(2200, im.width, im.height, self.get_target_path()))
        for sz in SIZES:
            target_size = get_size(im.width, im.height, sz)
            target = self.get_target_path(sz=sz)
            data.append(
                ImageDataRow(sz, target_size[0], target_size[1], target))
        return data

    def build_images(self, im):
        target = self.get_target_path()
        if Path(target).exists():
            return
        ensure_parent_dir(target)
        # orig = TemporaryFile()
        if im.mode == "CMYK":
            im = im.convert("RGB")
        im.save(target, self.format, optimize=True)
        # print('Image saved {}'.format(target))
        for sz in SIZES:
            target = self.get_target_path(sz=sz)
            t = im.copy()
            t.thumbnail((sz, sz), Image.BICUBIC)
            # f = TemporaryFile()
            t.save(target, self.format, optimize=True)

    def get_data(self):
        im_path = self.path.as_posix()
        # print('+image {}'.format(im_path))

        if is_image(self.path):
            with Image.open(im_path) as src_im:
                im = img_orientation(src_im)
                self.format = im.format
                self.build_images(im)
                data = self.build_data(im)
                return data

        raise ImageError('NotAnImage {}'.format(im_path))


#

compiler = Compiler()


def slugify(s):
    slug = unicodedata.normalize('NFKD', s)
    slug = slug.lower()
    slug = re.sub(r'[^a-z0-9]+', '_', slug).strip('_')
    slug = re.sub(r'[_]+', '_', slug)
    return slug


def include(context, key, filename):
    # filename = fn(context[key])
    try:
        with open(filename) as f:
            content = f.read()
            context[key] = markdown(content)
    except Exception as ex:
        print('Failed to include {} \n {}'.format(filename, ex))
        context[key] = ''


def link(context, key, fn):
    link_path = fn(context[key])
    context[key] = link_path


def norm_ID(bad_id):
    return int(bad_id[3:])


def pad(in_, nb, f='0'):
    s = str(in_)
    return s.rjust(nb, f)


def graph_id(nid):
    return 'ID{}'.format(pad(nid, 6))


def txt_id(nid):
    return 'ID{}'.format(pad(nid, 6))


MEDIA_CSS_TPL = """
@media (max-width: {media_width}px) {{
  #{media_id} {{
    background-image: url("{image_path}");
    padding-top: {image_ratio}%;
  }}
}}
"""


def media_css(this, options, medias):
    css = []
    for media in medias:
        for row in media['rows']:
            if row.h / row.w * 100 < 75:
                ratio = row.h / row.w * 100

            else:
                ratio = 75

            row_css = MEDIA_CSS_TPL.format(
                media_width=row.sz,
                media_id=media['id'],
                image_path=row.path,
                image_ratio=ratio)
            css.append(row_css)
    return '\n'.join(css)


# KKK (nuttige soort voor een hele reeks insecten en vogels)
extractKKtruc_re = re.compile(r"^(.+) \(.+")


def format_Kscore(this, options, s):
    # print('getscore {}, {},{}'.format(this, options, s))
    score = ''
    m = re.match(extractKKtruc_re, s)
    if m is None:
        return score

    g, = m.groups()
    c = g[0]
    if 'K' == c:
        score = '+' * len(g)

    elif '/' == c:
        score = '-' * len(g)

    return score


extractKKbidule_re = re.compile(r".+ \((.+)\)")


def format_Klabel(this, options, s):
    Klabel = 'ø'
    m = re.match(extractKKbidule_re, s)
    if m is None:
        return Klabel

    g, = m.groups()
    return g


def if_not_empty(this, options, s):
    if type(s) == list or type(s) == str:
        if len(s) > 0:
            return options['fn'](this)
        else:
            return options['inverse'](this)
    else:
        return options['inverse'](this)


def make_context(row):
    context = dict()
    for key in row:
        clean_key = slugify(key).upper()
        # print('Key({}) -> {}'.format(key, clean_key))
        context[clean_key] = row[key]

    return context


def make_helpers(with_media):
    helpers = dict(
        Kscore=format_Kscore,
        Klabel=format_Klabel,
        if_not_empty=if_not_empty,
    )

    if with_media:
        helpers['media_css'] = media_css
    else:

        def mc(a, b, c):
            return 'no medias'

        helpers['media_css'] = mc

    return helpers


photomatonre = re.compile(r".+PhotoMaton01.+")


def photomaton(img_name):
    return re.match(photomatonre, img_name) is not None


def listmedias(root_dir, dir_name):
    filenames = filter(lambda x: x.is_dir() is False,
                       map(Path,
                           root_dir.joinpath('Database-Media',
                                             dir_name).glob('*')))
    return sorted(filenames)


## << graph


def get_objects(node, pred):
    ks = node.keys()
    results = []
    for k in ks:
        np = node[k]['p']
        if np == pred:
            results.append(val(k))

    return results


def get_first_object(node, pred, default=''):
    results = get_objects(node, pred)
    if len(results) > 0:
        return results[0]

    return default


Obj = namedtuple('Obj', ['id', 'value'])


def obj(val):
    return Obj(uuid4(), val)


def val(o):
    return o.value


## graph >>

## << nav

WoodPoint = namedtuple('WoodPoint', ['x', 'y'])
SituatedWood = namedtuple('SituatedWood', ['id', 'wp'])
RelatedWood = namedtuple('RelatedWood', ['id', 'wp', 'dist'])
Stem = namedtuple('Stem', ['id', 'start', 'end'])


def get_coords(row):
    x = float(row['LAMBERT_1'][2:])
    y = float(row['LAMBERT_2'][2:])
    return WoodPoint(x, y)


def dist(p0, p1):
    dx = p1[0] - p0[0]
    dy = p1[1] - p0[1]
    return math.sqrt((dx * dx) + (dy * dy))


def add(v1, v2, a):
    t = a / dist(v1, v2)
    rx = v1[0] + (v2[0] - v1[0]) * t
    ry = v1[1] + (v2[1] - v1[1]) * t
    return WoodPoint(rx, ry)


def order_woods(ref, woods):
    rs = [RelatedWood(w.id, w.wp, dist(ref.wp, w.wp)) for w in woods]
    rs.sort(key=lambda rw: rw.dist)
    return rs


def arrow_stems(ref, woods, n, r_start, r_end):
    ws = order_woods(ref, woods)[1:1 + n]
    stems = []
    for w in ws:
        try:
            start = add(ref.wp, w.wp, r_start)
            end = add(ref.wp, w.wp, r_end)
            # print('{} {} {}'.format(w, start, end))
            stems.append(Stem(w.id, start, end))
        except Exception as ex:
            print(str(ex))
    return stems


def center_on(ref, wood):
    d = dist([0, 0], ref.wp)
    p = add(wood.wp, [0, 0], d)
    return SituatedWood(wood.id, p)


def set_arrow_end(s):
    u = 0.7
    ps = ['M', (0, 0), 'L', (4 * u, u), 'L', (0, 2 * u), 'z']
    p = svg.path.Path(d=ps, stroke='none')

    d = svg.container.Defs()
    m = svg.container.Marker(
        refX=u / 2,
        refY=u,
        orient='auto',
        markerWidth=u * 4,
        markerHeight=u * 2)
    m.add(p)
    d.add(m)
    s.add(d)

    return m


def get_nav(ref, woods):
    stems = arrow_stems(ref, woods, 6, 10, 32)
    d = svg.Drawing(
        '{}.svg'.format(ref.id),
        size=('100%', '100%'),
        viewBox='0 0 100 100',
    )
    marker = set_arrow_end(d)
    g = svg.container.Group()
    g.translate(50, 50)
    g.scale(1, -1)
    for stem in stems:
        hl = svg.container.Hyperlink(
            href='id_{}.html'.format(pad(stem.id, 3)), target='_top')
        l0 = d.line(stem.start, stem.end, stroke='black', stroke_width=1)
        l1 = d.line(stem.start, stem.end, stroke='white', stroke_width=2.5)
        l0['marker-end'] = marker.get_funciri()
        l0.translate(-ref.wp.x, -ref.wp.y)
        l1['marker-end'] = marker.get_funciri()
        l1.translate(-ref.wp.x, -ref.wp.y)
        hl.add(l1)
        hl.add(l0)
        g.add(hl)

    d.add(g)

    return d.tostring()


## nav >>


def url_default_label(url):
    p = urlparse(url)
    return p.netloc


def main():
    root_dir = Path(sys.argv[1])
    tpl_path = Path(sys.argv[2])
    out_dir = Path(sys.argv[3])
    with_media = True
    limit = None
    db_path = root_dir.joinpath('table.csv')
    graph_path = root_dir.joinpath('graph.csv')

    if len(sys.argv) > 4:
        limit = int(sys.argv[4])
    if out_dir.exists() is False:
        out_dir.mkdir(parents=True)

    G = nx.Graph()
    with open(graph_path.as_posix()) as f:
        r = csv.DictReader(f)
        for row in r:
            G.add_edge(
                row['SUJET'], obj(row['OBJET']), p=row['PREDICAT'].strip())

    def compute_desc_fr(rid):
        return root_dir.joinpath('fichiersTXT', '{}-Description-FR.txt'.format(
            txt_id(rid))).as_posix()

    def compute_desc_nl(rid):
        return root_dir.joinpath('fichiersTXT', '{}-Description-NL.txt'.format(
            txt_id(rid))).as_posix()

    def get_media_caption(name):
        try:
            caption_nl = get_first_object(G[name], 'caption nl')
            caption_fr = get_first_object(G[name], 'caption fr')

            # if len(caption_fr) > 0 and len(caption_nl) == 0:
            #     caption_nl = caption_fr

            # if len(caption_nl) > 0 and len(caption_fr) == 0:
            #     caption_fr = caption_nl

            # print('get_media_caption({}) [{}, {}]'.format(
            #     name, caption_fr, caption_nl))

            return dict(
                caption_fr=caption_fr,
                caption_nl=caption_nl,
            )
        except Exception as ex:
            return False

    def get_medias(clean_id):
        dir_name = pad(clean_id, 6)
        filenames = listmedias(root_dir, dir_name)
        media_outdir = out_dir.joinpath(dir_name)
        medias = []
        photomatons = []

        def adjust_path(i):
            return ImageDataRow(i.sz, i.w, i.h,
                                Path(i.path).relative_to(out_dir).as_posix())

        for f in filenames:
            try:
                wi = WebImage(f, media_outdir)
                rows = [r for r in map(adjust_path, wi.get_data())]
                data = dict(
                    id=slugify("id_{}".format(f.as_posix())),
                    image_url=rows[0].path,
                    rows=rows)
                captions = get_media_caption(f.name)
                if captions is not False:
                    data.update(captions)

                data['AVEC_CAPTION'] = captions is not False

                if photomaton(f.name):
                    photomatons.append(data)
                else:
                    medias.append(data)
            except ImageError:
                pass

        return dict(medias=medias, photomatons=photomatons)

    def get_lien_patrimoine(nid, scope, lang):
        """
        usage:
        {{#each LIEN_PATRIMOINE_REGIONAL_FR}}
        <div><a href="{{url}}">{{label_fr}}</a></div>
        {{/each}}
        """
        results = []
        gid = graph_id(nid)
        try:
            node = G[gid]
            urls = get_objects(node, 'lien patrimoine {} {}'.format(
                scope, lang))
            for url in urls:
                results.append(dict(url=url))
        except Exception:
            pass

        return results

    def get_lien_jumelage(nid, scope):
        """
        usage:
        {{#each LIEN_JUMELAGE_BELGIQUE}}
        <div><a href="{{url}}">{{label_fr}}</a></div>
        {{/each}}
        """
        results = []
        gid = graph_id(nid)
        try:
            node = G[gid]
            urls = get_objects(node, 'lien jumelage {}'.format(scope))
            for url in urls:
                label_fr = get_first_object(G[url], 'label url fr')
                label_nl = get_first_object(G[url], 'label url nl')

                if label_fr is '':
                    label_fr = url

                if label_nl is '':
                    label_nl = url

                results.append(
                    dict(url=url, label_fr=label_fr, label_nl=label_nl))
        except Exception:
            pass

        if len(results) > 0:
            return results
        return False

    def get_linked_medias(nid):
        results = []
        gid = graph_id(nid)
        try:
            node = G[gid]
            urls = get_objects(node, 'lien media')
            for url in urls:
                label_fr = get_first_object(G[url], 'label url fr')
                label_nl = get_first_object(G[url], 'label url nl')

                if label_fr is '':
                    label_fr = url

                if label_nl is '':
                    label_nl = url

                results.append(
                    dict(url=url, label_fr=label_fr, label_nl=label_nl))
        # print('get_linked_medias({}) => {}'.format(nid, len(results)))
        except Exception:
            pass

        if len(results) > 0:
            return results
        return False

    def get_planche_bota(name):
        results = []
        try:
            node = G[name]
            img_names = get_objects(node, 'planche_botanique')
            for name in img_names:
                if name is not '':
                    label_fr = get_first_object(G[name], 'caption fr')
                    label_nl = get_first_object(G[name], 'caption nl')
                    results.append(
                        dict(
                            name=name,
                            caption_fr=label_fr,
                            caption_nl=label_nl))
        except Exception:
            print('Failed getting planche_bota for {}'.format(name))
        return results

    def get_embed_media(nid, scope):
        results = []
        gid = graph_id(nid)
        try:
            node = G[gid]
            embeds = get_objects(node, 'embed media {}'.format(scope))
            for embed in embeds:
                results.append(dict(embed=embed))
        except Exception:
            pass

        return results

    def get_remarqueur_name(nid):
        remarqueur = dict(
            REMARQUEUR_NAME_FR=False,
            REMARQUEUR_NAME_NL=False,
        )
        try:
            gid = graph_id(nid)
            node = G[gid]
            remarqueur['REMARQUEUR_NAME_FR'] = get_first_object(
                node, 'remarqueur name fr')
            remarqueur['REMARQUEUR_NAME_NL'] = get_first_object(
                node, 'remarqueur name nl')
        except Exception as ex:
            print('ERROR get_remarqueur_name({}): {}'.format(gid, ex))
            pass

        # print('get_remarqueur_name({}): {}'.format(graph_id(nid), remarqueur))
        return remarqueur

    def get_remarqueur_url(nid):
        remarqueur = dict(
            REMARQUEUR_URL_FR=False,
            REMARQUEUR_URL_NL=False,
        )
        try:
            gid = graph_id(nid)
            node = G[gid]
            remarqueur['REMARQUEUR_URL_FR'] = get_first_object(
                node, 'remarqueur url fr')
            remarqueur['REMARQUEUR_URL_NL'] = get_first_object(
                node, 'remarqueur url nl')
        except Exception:
            pass

        return remarqueur

    def get_activite(nid):
        gid = graph_id(nid)
        try:
            node = G[gid]
            results_fr = []
            urls_fr = get_objects(node, 'activite url fr')
            for url in urls_fr:
                try:
                    label = get_first_object(G[url], 'label url fr', url)
                    results_fr.append(dict(url=url, label=label))
                except KeyError:
                    results_fr.append(
                        dict(url=url, label=url_default_label(url)))

            results_nl = []
            urls_nl = get_objects(node, 'activite url nl')
            for url in urls_nl:
                try:
                    label = get_first_object(G[url], 'label url nl', url)
                    results_nl.append(dict(url=url, label=label))
                except KeyError:
                    results_nl.append(
                        dict(url=url, label=url_default_label(url)))

            return dict(ACTIVITE_FR=results_fr, ACTIVITE_NL=results_nl)

        except Exception:
            return dict()

    def get_portrait(nid):
        portrait = dict(
            PORTRAIT_FR='',
            PORTRAIT_NL='',
        )
        pdir = root_dir.joinpath('portrait')
        try:
            gid = graph_id(nid)
            node = G[gid]
            fr = pdir.joinpath(get_first_object(node, 'portrait fr'))
            with open(fr.as_posix()) as f:
                portrait['PORTRAIT_FR'] = markdown(f.read())

            nl = pdir.joinpath(get_first_object(node, 'portrait nl'))
            with open(nl.as_posix()) as f:
                portrait['PORTRAIT_NL'] = markdown(f.read())
        except Exception:
            pass

        return portrait

    template = None
    with open(tpl_path.as_posix()) as f:
        template = compiler.compile(f.read())

    if template is None:
        raise Exception('failed to build template')

    helpers = make_helpers(with_media)

    rows = None
    with open(db_path.as_posix()) as f:
        rows = list(csv.DictReader(f))

    if limit is not None:
        rows = rows[:limit]

    if rows is not None:
        list_ = []

        woods = []
        for row in rows:
            context = make_context(row)
            rid = norm_ID(context['ID'])
            woods.append(SituatedWood(rid, get_coords(context)))

        for row in rows:
            context = make_context(row)
            context['SID'] = slugify(context['ID'])
            partials = dict()
            rid = norm_ID(context['ID'])
            print('Processing {}'.format(rid))
            if with_media:
                context.update(get_medias(rid))

            ref = SituatedWood(rid, get_coords(context))
            partials['nav'] = compiler.compile(get_nav(ref, woods))

            context.update(get_portrait(rid))
            context.update(get_remarqueur_name(rid))
            context.update(get_remarqueur_url(rid))
            context.update(get_activite(rid))

            for scope in ['regional', 'national', 'mondial']:
                for lang in ['fr', 'nl']:
                    ks = 'LIEN_PATRIMOINE_{}_{}'.format(
                        scope.upper(), lang.upper())
                    context[ks] = get_lien_patrimoine(rid, scope, lang)

            ljn = False
            for scope in ['belgique', 'europe', 'monde']:
                ks = 'LIEN_JUMELAGE_{}'.format(scope.upper())
                context[ks] = get_lien_jumelage(rid, scope)
                if context[ks] is not False:
                    ljn = True
            context['LIEN_JUMELAGE'] = ljn

            for scope in ['youtube', 'vimeo', 'roundme']:
                ks = 'EMBED_MEDIA_{}'.format(scope.upper())
                context[ks] = get_embed_media(rid, scope)

            context['LINKED_MEDIAS'] = get_linked_medias(rid)

            context['PLANCHE_BOTA'] = get_planche_bota(context['LATIN'])

            context['PDF_LINK'] = '{}.pdf'.format(slugify(context['ID']))

            try:
                context['is_remarquable'] = context[
                    'CATE_GORIE'].lower() == 'remarquable'
            except KeyError:
                context['is_remarquable'] = False

            include(context, 'TRAITS_CARACTE_RE_DE_L_INDIDIVU',
                    compute_desc_fr(rid))
            include(context, 'KENMERKEN_KARAKTER_VAN_HET_INDIVIDU',
                    compute_desc_nl(rid))

            # print('#{} ============================'.format(rid))
            # print(json.dumps(context, indent=2))
            content = template(context, helpers, partials)
            dest_path = out_dir.joinpath('{}.html'.format(
                slugify(context['ID'])))

            with open(dest_path.as_posix(), 'w') as dest:
                dest.write(content)
            list_.append(dest_path.relative_to(out_dir).as_posix())

        with open(out_dir.joinpath("index.html").as_posix(), 'w') as index:
            for p in list_:
                index.write("<p><a href=\"{}\">{}</a></p>".format(p, p))


if __name__ == '__main__':
    main()
