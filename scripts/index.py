#!/usr/bin/env python3

import os
import math
import csv
import sys
from pathlib import Path
from pybars import Compiler


def main():
    root_dir = Path(sys.argv[1])
    tpl_path = Path(sys.argv[2])
    dest_path = Path(sys.argv[3])
    db_path = root_dir.joinpath('table.csv')

    compiler = Compiler()

    rows = None
    with open(db_path.as_posix()) as f:
        rows = list(csv.DictReader(f))

    if rows is None:
        raise Exception('failed to get rows')

    template = None
    with open(tpl_path.as_posix()) as f:
        template = compiler.compile(f.read())

    if template is None:
        raise Exception('failed to build template')

    context = dict(TREE_COUNT=len(rows))
    content = template(context)
    with open(dest_path.as_posix(), 'w') as dest:
        dest.write(content)


if __name__ == '__main__':
    main()
