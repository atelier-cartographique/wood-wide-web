#!/usr/bin/env python3

from odf.opendocument import load
import sys
from pathlib import Path

def main():
    base_dir = Path(sys.argv[1])
    print("base dir = {}".format(base_dir))

    for fn in  base_dir.glob('*.odt'):
        doc = load(fn.as_posix())
        print("====Processing {} ============".format(fn))
        outfn = base_dir.joinpath('ID' + fn.name[:-3] + 'txt')
        outfile = open(outfn.as_posix(), 'w')
        for n in doc.text.childNodes:
            outfile.write(str(n))
        outfile.close()


if __name__ == '__main__':
    main()
