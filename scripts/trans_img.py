#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from math import floor
from PIL import Image
from colorsys import rgb_to_hsv, hsv_to_rgb
from glob import glob
from multiprocessing import Pool

def to_rgb(h, s, v):
    r, g, b = hsv_to_rgb(h, s, v)
    return (int(floor(r)), int(floor(g)), int(floor(b)))

def scale_sat(h):
    # option B
    center = 0.45
    d = (1 - abs(center - h)) - 0.3
    return (d / 0.28) + 0

    # option A
    # center = 0.45
    # d = (1 - abs(center - h)) - 0.3
    # return (d / 0.2) + 0

    # option de départ
    # center = 0.4
    # d = (1 - abs(center - h)) - 0.4
    # return (d / 0.6) + 0.6
    # return max((d * max_d / max_sat) * 100, 1)

def trans(p):
    h, s, v = rgb_to_hsv(p[0], p[1], p[2])
    return to_rgb(h, s * scale_sat(h), v)

def process_img(source_fn):
    print('Processing {}'.format(source_fn))
    source = Image.open(source_fn)
    target = source.copy()

    for y in range(source.height):
        for x in range(source.width):
            p = source.getpixel((x,y))
            nc = trans(p)
            try:
                target.putpixel((x,y), nc)
            except TypeError as ex:
                print('Err %s' % (nc,))
                raise ex
    
    source.close()
    target.save(source_fn)
    print('Done {}'.format(source_fn))



def main():
    pat = sys.argv[1]
    
    with Pool() as p:
        p.map(process_img, glob(pat))

if __name__ == '__main__':
    main()
