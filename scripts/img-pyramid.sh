#!/bin/bash
# by Pacôme Béru - Atelier Cartographique
# build image pyramid from jpgs in current TARGET_DIR

SOURCE_DIR=$1
TARGET_DIR=$2/"pyramid"

if [ ! -d "$TARGET_DIR" ]; then
    mkdir -p $TARGET_DIR
fi
if [ ! -d "$TARGET_DIR/landscape" ]; then
    mkdir -p $TARGET_DIR/landscape
fi
if [ ! -d "$TARGET_DIR/portrait" ]; then
    mkdir -p $TARGET_DIR/portrait
fi

# Loop through all JPGs and PNGs in current TARGET_DIR, test if directories and files exists, if not, create missing items.
i=200

while [[ $i -le 2400 ]]; do

    if [[ ! -d "$TARGET_DIR/landscape/$i" ]]; then
        mkdir $TARGET_DIR/landscape/$i
    fi
    if [[ ! -d "$TARGET_DIR/portrait/$i" ]]; then
        mkdir $TARGET_DIR/portrait/$i
    fi

    for sf in ${SOURCE_DIR}/*.jpg; do
        echo "Processing ${sf}"
        f=$(basename ${sf})
        if [[ ! -f "$TARGET_DIR/landscape/$i/$f" ]]; then
            convert ${sf} -resize $i $TARGET_DIR/landscape/$i/${f}
        fi
        if [[ ! -f "$TARGET_DIR/portrait/$i/$f" ]]; then
            convert ${sf} -geometry x$i $TARGET_DIR/portrait/$i/${f}
        fi
    done

    for sg in ${SOURCE_DIR}/*.png; do
        echo "Processing ${sg}"
        g=$(basename ${sg})
        if [[ ! -f "$TARGET_DIR/landscape/$i/$g" ]]; then
            convert ${sg} -resize $i $TARGET_DIR/landscape/$i/${g}
        fi
        if [[ ! -f "$TARGET_DIR/portrait/$i/$g" ]]; then
            convert ${sg} -geometry x$i $TARGET_DIR/portrait/$i/${g}
        fi
    done


    let i=$i+200
done
