#!/usr/bin/env python3
import sys
import csv
import unicodedata
import re
from json import dumps
from pathlib import Path
from shapely.geometry import Point
import networkx as nx


class GeoAcc:
    features = []

    def push(self, coords, props):
        p = Point(coords).buffer(10)
        self.features.append(
            dict(
                type='Feature',
                geometry=dict(
                    type="Polygon", coordinates=[list(p.exterior.coords)]),
                properties=props))

    def geojson(self):
        coll = dict(type="FeatureCollection", features=self.features)

        return dumps(coll)


def get_coords(row):
    x = float(row['lambert_1'][2:])
    y = float(row['lambert_2'][2:])

    return (x, y)


def norm_ID(bad_id):
    return int(bad_id[3:])


def pad(in_, nb, f='0'):
    s = str(in_)
    return s.rjust(nb, f)


def graph_id(nid):
    return 'ID{}'.format(pad(nid, 6))


def slugify(s):
    slug = unicodedata.normalize('NFKD', s)
    slug = slug.lower()
    slug = re.sub(r'[^a-z0-9]+', '_', slug).strip('_')
    slug = re.sub(r'[_]+', '_', slug)
    return slug


def make_context(row):
    context = dict()
    for key in row:
        clean_key = slugify(key)
        context[clean_key] = row[key]

    return context


## << graph


def get_objects(node, pred):
    ks = node.keys()
    results = []
    for k in ks:
        np = node[k]['p']
        if np == pred:
            results.append(k)

    return results


def get_first_object(node, pred):
    results = get_objects(node, pred)
    if len(results) > 0:
        return results[0]

    return ''


## graph >>


def main():
    for arg in sys.argv:
        print('# {}'.format(arg))
    root_dir = Path(sys.argv[1])
    out_dir = Path(sys.argv[2])

    db_path = root_dir.joinpath('table.csv')
    graph_path = root_dir.joinpath('graph.csv')

    G = nx.Graph()
    with open(graph_path.as_posix()) as f:
        r = csv.DictReader(f)
        for row in r:
            if 'portrait fr' == row['PREDICAT']:
                print('EDGE({}, {}, {})'.format(row['SUJET'], row['PREDICAT'],
                                                row['OBJET']))
            G.add_edge(row['SUJET'], row['OBJET'], p=row['PREDICAT'])

    def has_portrait(nid):
        gid = graph_id(nid)
        try:
            node = G[gid]
            if node is None:
                print('No Node For {}'.format(nid))
                return False

            fr = get_first_object(node, 'portrait fr')
            nl = get_first_object(node, 'portrait nl')

            # print('{} -> portrait fr({}), nl({})'.format(gid, fr, nl))
            if fr == '' or nl == '':
                return False
        except Exception:
            return False

        return True

    if out_dir.exists() is False:
        out_dir.mkdir(parents=True)

    geo_acc = GeoAcc()
    with open(db_path.as_posix()) as f:
        r = csv.DictReader(f)
        for row in r:
            context = make_context(row)
            rid = norm_ID(context['id'])
            context['wood_file'] = '{}.html'.format(slugify(context['id']))
            context['has_portrait'] = has_portrait(rid)
            geo_acc.push(get_coords(context), context)
            print('Done {}'.format(rid))

    with open(out_dir.joinpath('geo.ts').as_posix(), 'w') as geojs:
        print('Gathering data to write into {}'.format(
            out_dir.joinpath('geo.ts').as_posix()))
        data = """
        import {{ FeatureCollection }} from "geojson-iots";
        const data: FeatureCollection = {geojson};
        export default data;
        """.format(geojson=geo_acc.geojson())
        geojs.write(data)


if __name__ == '__main__':
    main()
