# L’Arbre Rond-Point

## Marqueur de paysage
Le Tilleur des Invalides a été planté dans les années 30, non pas au milieu d’un champs, mais au centre d’un rond-point. Tout tourne autour de lui : des milliers de véhicules chaque jour, les tondeuses de la Commune, les chiens qui se promènent avec leur maître.
Cet arbre appartient à une espèce particulièrement résistante à la pollution de l’air et à la sécheresse du sol. Il pousse au cœur de ce tourbillon : comme imperturbable au chaos environnant.
Il a le port typique de ces tilleuls isolés dans la campagne qui grandissent sans concurrence d’autres arbres. Il a pu développer une couronne majestueuse, ample, étalée, bien équilibrée et dense au-dessus du trafic. Il est devenu rond-point à lui tout seul.

## Marqueur temporel
Depuis la Révolution française, les tilleuls sont souvent plantés lors d’événements historiques ou leur commémoration. Ce geste permet de graver une date à travers les époques car ces arbres sont capables de vivre plus de 1000 ans. C’est ainsi que plusieurs dizaines de milliers de tilleuls en France symbolisent la Liberté, l’Egalité et la Fraternité.

Cet individu ci, lui, a été mis en terre en 1930 pour commémorer le centenaire de l’Indépendance de la Belgique. Un lien historique fort discret :  rares sont les passants qui s’aventurent à traverser le rond-point et découvrent la plaque commémorative au pied de l’arbre. Aujourd’hui ce tilleul est un monument en soi. Il semble plus célébrer l’« Arbre en ville » que  la naissance de la Belgique.
Marqueur sensoriel
Les tillia tomentosa sont assez courants à Bruxelles. En latin « tomentosa » signifie « poilu ». Car l’envers des feuilles de ces tilleuls est couvert d’un fin duvet blanc. Il s’irise sous les rayons du soleil. Les reflets argentés de cet arbre animent son feuillage vert assez sombre et mat. Cette caractéristique est soulignée par le nom scientifique français : tilleul « argenté ».

A l’automne, ce grand feuillu se pare d’un jaune profond et chaleureux. Une robe qu’il garde longtemps. Généralement ses effets de couleur et de lumière ne passent pas inaperçus.

Ce spectacle est encore plus frappant chez le tilleul des Invalides car il est visible de toutes parts et il se reflète dans toutes les fenêtres des immeubles qui l’entourent.
Son image se love dans la mémoire collective et, mine de rien, marque les esprits des d’automobilistes et des habitants.

L’arbre ne se contente pas de caresser leurs yeux. Il filtre la pollution et embaume tout le quartier. En juillet, le parfum de sa floraison blanche, est très intense, suave, presque capiteux. Si bien qu’il parvient à faire oublier les odeurs des pots d’échappement. Et lorsque le trafic se calme, le son des quelques oiseaux qui l’habitent est une musique douce pour l’oreille. Cet arbre a de quoi nous faire tourner la tête.

## Pas si immobile que ça
Le Tilleul des Invalides semble d’autant plus figé que tout s’agite autour de lui. Pourtant, il n’est pas aussi statique qu’il en a l’air.  En effet, ses graines sont faites pour être emportées par le vent. Chaque graine possède une bractée ailée : une sorte de feuille très légère. Cette petite « voile » leur permettent de s’envoler comme de mini hélicoptères, de tournoyer dans les vents d’automne, et de se disperser dans le quartier.
D’autre part, cet arbre émet des messages à sa façon. Ses fleurs parfumées attirent les abeilles à plusieurs centaines de mètres à la ronde en émettant des molécules organiques volatiles : ces molécules sont captées par les pollinisateurs.
