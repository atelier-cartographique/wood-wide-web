﻿"Onzen hof/Notre jardin"
Ontdek de kleine verhalen van het park

De Schaarbeekoise, Carla Vanparys, verzamelt in een prachtig tweetalig boek over het Josaphatpark "Onzen hof / onze tuin" een reeks portretten in woord en beeld van verschillende trouwe arbeiders en bezoekers van één van de mooiste parken van Brussel.

Boom Bomer Boomst

"soms voel ik me het park
soms
voel ik geen verschil tussen mij en de boom
en de bladeren en het gras
alsof ik het was"

Carla kan geen boom plukken: "o o o ik kan niet één boom kiezen ik wil niet kiezen ik hou van ze allemaal"

Inschrijvingen:  onzenhof.notrejardin@gmail.com