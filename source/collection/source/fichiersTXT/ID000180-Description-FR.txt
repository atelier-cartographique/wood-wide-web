Dans un bosquet du Jardin Botanique de Bruxelles, poussait un Davidia magnifique. 
On ne le remarquait qu'au mois de mai, lorsqu'il se paraît de blanc. Cet individu a été choisi par le jardinier-botaniste Guillaume Mamdy. Mais il a mystérieusement disparu. Reste un lien invisible qui le relie à un arbre jumeau du Jardin Botanique de Meise.Si vous voulez assister au spectacle de sa floraison, c'est là-bas, que vous pourrez le trouver.

Guillaume Mamdy, raconte que cette espèce originaire de Chine est unique en son genre. Elle est d’une grande rareté: non seulement chez nous, mais aussi dans son milieu naturel en Chine, surtout les vieux sujets. 
Le « Davidia » est arrivé en Europe pour la première fois en 1897 grâce à un missionnaire naturaliste français : Armand David. C’est à lui que l’arbre doit son nom. Cet homme a découvert et décrit de nombreuses espèces animales et végétales originaires de Chine. Il est notamment le premier à avoir décrit le grand panda de Chine. Ici, vous ne trouverez pas de panda dans les branches des davidias.
Mais ces arbres, très peu communs chez nous, méritent le déplacement. Car au mois de mai, ils offrent un étrange spectacle. Leurs fleurs ont l’air de mouchoirs suspendus dans son feuillage : les petites fleurs mauves sont entourées de grandes bractées couleur crème qui font penser à des fantômes. 
D’où le nom commun de cet arbre : « Arbre aux mouchoirs » ou « Arbre aux fantômes ». 
Un arbre qui porte particulièrement bien son nom à Bruxelles.Sur les cartes il existe 3 davidias remarquables. Celui du Jardin Botanique et celui du Parc Tenbosch se sont mystérieusement volatilisés. Reste celui du Jardin Botanique Massart.

Comme cet arbre tisse des liens entre plusieurs jardins botaniques belges, nous l’avons surnommé: « L’arbre botanicum » 


