﻿Ce petit bois de 14 hêtres a été remarqué par la Maison CFC. Ils forment un ensemble :

" Placés de chaque côté du monument aux Martyrs de 1830, deux groupes de 14 hêtres taillés en cube.
Face à la librairie et aux bureaux de la Maison CFC, sous les pavés, se trouve la fosse commune où sont enterrés les 467 héros et martyrs des combats des « Journées de septembre 1830 », durant lesquelles s’est jouée l’Indépendance de la Belgique. C’est dans cet humus « historique » que s’enracinent 14 des 28 hêtres de la place.

La taille en forme de cube, ou de boîte, de ces petits arbres qui dispensent une ombre parcimonieuse, donne à songer, par association, au nichoir, installé dans l’arbre, une boîte dans la boîte, un logis miniature… mais aussi, telle une poupée gigogne, au livre posé sur les tables de la Maison CFC, une Maison d’édition et une Librairie.

Le livre, un objet, un lieu en soi. Un lieu pour se retirer, pour rêver, pour lire, mais aussi pour réfléchir, traverser le temps, regarder et penser, à la manière du  Baron perché , ce célèbre personnage de l’écrivain italien Italo Calvino qui décide, à douze ans, de grimper au chêne du jardin familial pour ne plus en descendre et qui, loin de vivre en ermite, saute de branche en branche et s’élance à la découverte du monde : « tout se passait comme si, plus il était décidé à rester caché dans ses branches, plus il se sentait le besoin de créer de nouveaux rapports avec le genre humain . "



