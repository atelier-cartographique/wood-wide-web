﻿Pour fêter « Mai 68 », la Vénerie a choisi un tilleul, arbre qui symbolise la Liberté depuis la Révolution française. Ce grand individu à larges feuilles a connu toutes les fêtes des fleurs. Tel l’arbre du village, il participe à la vie de la Commune et de ses habitants. Rassurant, il joue parfois le rôle de confident, de gardiens de la mémoire. Depuis septembre 2017, un registre est parfois placé au pied de ce géant : il recueille les pensées et les souvenirs des passants, petits et grands.
A l’occasion de la fête des fleurs 2018, le public est invité à déposer un petit mot au stand de Wood Wide Web ou dans le registre au pied de l’arbre place Bisschoffsheim.

Extraits:

#« Quand j’étais petit, je n’étais pas grand !  
#Le tilleul me paraissait gigantesque
#Je venais lui parler, l’écouter, l’admirer ! »

#« En chantant une musique mystérieuse,
#Des mots chuintés, des harmoniques végétales »

#« Il fit mon Maître, mon guide,
#celui que je pouvais venir consulter 
#et qui toujours me redisait ce que je devais entendre,   
#l’inflexion des voix dès qui se sont tues »

#« Même dans les moments les plus sombre, 
#on peut trouver le bouleau, 
#si on se rappelle d’allumer la lumière »

#« Belle fête autour du tilleul »

#« Ma commune d’adoption pleine de gens on fait que, 
#venir d’autres pays un peu forcé, 
#c’est devenu chez moi et je les remercie »

#« Y a-t-il de la place pour l’humain dans cette société actuelle ? »

#« Quelle splendeur, apprendre à les respecter car on en a besoin.
#Merci l’arbre »

#« Je suis tout petit devant cette puissance, 
#je pleure quand l’homme d’un coup de tronçonneuse met fin à sa vie, 
#quand l’homme arrêtera- t-il de vouloir tout régenter ?   
#Quand arrêtera-t-il de penser que c’est lui qui détient la vérité, 
#la décision de vie ou de mort ? 

#« Veillons à ce que nos rêves soient plus longs que nos nuits 
#Merci les arbres »