Majestueux, ce vieux frêne pleureur veille sur les tombes. Sa silhouette bienveillante embrasse la nécropole, l’ornant d'un rideau végétal à la belle saison, et conférant au lieu un romantisme mystérieux …
La Fondation CIVA, dans le cadre de son événement annuel « Garden tales », vous invite à découvrir ces lieux insolites et souvent méconnus que sont les cimetières. Lieux de repos des défunts, ce sont en même temps des lieux de vie pour d’innombrables arbres, souvent centenaires. Le frêne de Laeken est un de ces arbres emblématiques, marquant de sa silhouette singulière le caractère du lieu.

Ces 12 et 13 mai, 2 visites seront particulièrement « arborées » :
Cimetière Schaerbeek : visite avec une architecte paysagiste qui explicitera la conception particulièrement arborée de ce cimetière, conçu par le célèbre paysagiste René Pechère.
Cimetière de  Ganshoren : visite avec une guide nature qui éclairera tout particulièrement la symbolique des arbres et des végétaux.


