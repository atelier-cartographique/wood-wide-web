Cet arbre a été choisi par la bibliothèque Communale de Molenbeek dans le cadre de Lire dans les Parcs. Il fait partie des Arbre Lire de Bruxelles:
"Nous avons choisi cet arbre car il trône fièrement et majestueusement  au milieu du Parc du Karreveld. Il donne envie de s’arrêter, de s’installer à l’ombre de ses branches, pour lire un livre. C’est d’ailleurs ce platane que les conteuses choisissent pour raconter des histoires lors de l’activité Lire dans les parcs tous les jeudis de l’été 2018 de 14h-16h"

Si vous avez envie de feuilleter d'autres livres arborés, voici quelques ouvrages que vous trouverez à la bibliothèque de Molenbeek:


Titre	Que trouve-t-on à la campagne ? / par Gregor Aas, Marc Duquet, Edmund Garnweidner ... [et al.]
Auteur	Gregor Aas
Editeur	Paris : Nathan, 2001
Page	238 p. ; 22 cm
ISBN	2 09 260598-4 
Cote de rangement	Molenbeek - N°2 Charles Malis : [ADU-DOC] 58/59 QUET Q 

 
Titre	Pourquoi coupe-t-on les arbres ? / Anne-Sophie Baumann
Auteur	Anne-Sophie Baumann
Editeur	Paris : Tourbillon, 2006
Page	45 p. ; 25 cm
ISBN	2-84801-208-0 
Cote de rangement	Molenbeek - N°2 Charles Malis : [J-DOC] 674 BAUM P
 
 
Titre	Mon petit jardin / par Benoît Delalandre
Auteur	Benoît Delalandre
Editeur	Paris : Larousse, 2004
Page	93 p. ; 29 cm
ISBN	2 03 553057 1 
Cote de rangement	Molenbeek - N°2 Charles Malis : [J-DOC] 58 DEL M
 
 
Titre	Encyclopédie visuelle : Arbres et arbustes / par Maurice Dupérat  et Jean-Marie Polese
Auteur	Maurice Dupérat
Editeur	Paris : Artémis éditions, 2008
Page	239 p. ; 25 cm
ISBN	978-2-84416-341-7 
Cote de rangement	Molenbeek - N°2 Charles Malis : [ADU-DOC] 58 DUPE E
 
 
Titre	Arbres fruitiers / par James Gourier
Auteur	James Gourier
Editeur	Toulouse : Milan, 2000
Page	31 p. ; 19 cm
ISBN	2-7459-0001-3 
Cote de rangement	Molenbeek - N°2 Charles Malis : [J-DOC] 634.1 GOU A
 
 
Titre	Arbres des villes et des jardins / par James Gourier
Auteur	James Gourier
Editeur	Toulouse : Milan, 1997
Page	39 p. ; 20 cm
ISBN	2-84113-474-1 . - 2-7459-2029-4 
Cote de rangement	Molenbeek - N°2 Charles Malis : [J-DOC] 58 GOUR A
 
 
Titre	Le verger pas à pas / Jean-Marie Polese
Auteur	Jean-Marie Polèse
Editeur	Aix-en-Provence : Edisud, 2009
Page	95 p. ; 25 cm
ISBN	978-2-7449-0802-6 
Cote de rangement	Molenbeek - N°2 Charles Malis : [ADU-DOC] 634 POLE V


Titre	Les arbres / Colin Ridsdale, John White et Carol Usher
Auteur	Colin Ridsdale
Editeur	Paris : Gründ, 2006
Page	360 p. ; 23 cm
ISBN	2-7000-1447-2 
Cote de rangement	Molenbeek - N°2 Charles Malis : [ADU-DOC] 58 RID A
 
 
Titre	Les arbres / Tony Rodd et Jennifer Stackhouse
Auteur	Tony Rodd
Editeur	Toulouse : Milan, 2009
Page	304 p. ; 24 cm
ISBN	978-2-7459-3531-1 
Cote de rangement	Molenbeek - N°2 Charles Malis : [ADU-DOC] 582 RODD A
 
 
Titre	Des arbres / Olivier Rose
Auteur	Olivier Rose
Editeur	Nantes : Gulf Stream, 2005
Page	63 p. ; 22 cm
ISBN	2-909421-36-8 
Cote de rangement	Molenbeek - N°2 Charles Malis : [J-DOC] 634 ROS D
 
 
Titre	L'encyclopédie mondiale des arbres / Tony Russell et Catherine Cutler
Auteur	Tony Russell
Editeur	Paris : Hachette Livre, 2008
Page	256 p. ; 31 cm
ISBN	978-2-0123-5899-7 
Cote de rangement	Molenbeek - N°2 Charles Malis : [ADU-DOC] 582 RUSS E
 
 
Titre	Les arbres du jardin / Annette Schreiner
Auteur	Annette Schreiner
Editeur	Paris : Rustica, 2009
Page	191 p. ; 29 cm
ISBN	978-2-8403-8855-5 
Cote de rangement	Molenbeek - N°2 Charles Malis : [ADU-DOC] 582 SCHR A
 
 
Titre	Le monde extraordinaire des plantes et des arbres / Son Tyberg
Auteur	Son Tyberg
Editeur	Aartselaar : Chantecler, 1991
Page	125 p. ; 29 cm
ISBN	2 8034 2140 2 
Cote de rangement	Molenbeek - N°2 Charles Malis : [J-DOC] 58 TYB M
 
 
Titre	Fleurs, fruits et graines
Editeur	Mouans-Sartoux : Pemf, 2004
Page	23 p. ; 22 cm
ISBN	2 84526 518 2 
Cote de rangement	Molenbeek - N°2 Charles Malis : [J-DOC] 581 FLE F
 
"