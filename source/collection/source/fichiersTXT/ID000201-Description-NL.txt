﻿"Een stam, een volk.
Een familie van negen kastanjebomen.
En cirkel van wijze ingewijden die een doorgang aanbieden om diegenen te verwelkomen, die ervoor kiezen om naar hen te kijken. Als een poort die zich opent ter uitnodiging om naar hen te komen luisteren.
In de erkenning die wij hen geven, openbaren de kastanjebomen zich in hun oneindige wijsheid en hun majestueuze schoonheid.
Als het cijfer “negen” symbool staat voor het ideaal, de kennis en het spirituele, de onbaatzuchtigheid, de gevoeligheid en de edelmoedigheid, dan vertegenwoordigt deze cirkel van kastanjebomen de harmonie en het Licht van de Goddelijke Geest.
Wanneer we in deze cirkel binnenkomen, dan kunnen we de warmte van een cocon voelen, de zachtheid van een nest. We mogen ons neerzetten in de Vrede, en de frisheid van hun vriendelijke Adem ontvangen.
De kastanjebomen reiken ons hun takken met talloze open handen, en nodigen ons uit om te berusten in hun hart van diepe wortels.
Wanneer we onze blik richten naar de hoogte van hun toppen, dan ontwaart zich een trap van takken die ons tot de Hemel leiden, een wonderlijke doorgang naar het Zonlicht.
Tussen Hemel en Aarde, verzoeken de kastanjebomen ons om op de vleugels van onze goddelijke zuiverheid te reizen, om de alliantie met het Volk van de Bomen te verkennen en te herstellen.
Terwijl we bij hen voorbijgaan, waken zij discreet over ons. Ze zorgen voor iedereen zonder er iets in ruil voor terug te vragen, ze genezen wat genezing behoeft, ze zuiveren wat zuivering behoeft. Ze schenken ons de zuurstof voor elke ademhaling en de gift van licht in heel hun wezen. 
Ondanks hun verwondingen, hun lijden en onze achteloosheid, staan ze trots rechtop. Ze vinden hun kracht in hun Bondgenootschap en, in welwillendheid, glimlachen ze bij onze onwetendheid.
Kastanjebomen spelen graag met de vogels. Ze vertrouwen hen boodschappen toe die in de wind als gezangen worden meegevoerd tot een wonderlijk concert. Zij verwelkomen hun nesten in hun takken. Om vervolgens op hen toe te zien, en hen met hun groene kruin te beschermen en hen met hun tedere liefde te omhullen. 
Als de nacht valt, en de stilte en vrede heersen, zijn de kastanjebomen ware koningen die zich verrukken in de fonkeling van de sterren. Ze dansen in de wind en zingen voor hun Vader en Moeder. Ze waken terwijl de mensen slapen en herstellen de harmonie in de omgeving.". Corine Moëns, "La Voix de la Forêt"
