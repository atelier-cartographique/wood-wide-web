CIVA plaatst de gewone es (Fraxinus excelsior) in het middelpunt van de belangstelling op haar evenement Garden Tales. "Het is een knipoog naar dezelfde soort die sterk aangroeit aan de achterkant van CIVA en de bureaus van haar departement Tuin, Landschap en Stedelijk Ecosysteem.  
De gewone es is een soort die bedreigd wordt door de chalara fraxinea, een schimmel (Hymenoscyphus fraxineus) afkomstig uit Azië.

Tijdens de lancering van Garden Tales 2018 in het weekend van 12 en 13 mei, waarin de landschapsarchitectuur van begraafplaatsen centraal stond, kozen we een majestueuze treures als boom (de Fraxinus excelsior ‘Pendula’), als bewaker van het grafpatrimonium op de begraafplaats van Laken.

Voor het laatste weekend van Garden Tales 2018 zetten we opnieuw een treures in de kijker op het Avijl Plateau. Deze Fraxinus excelsior, nobel, groot en met een landelijk karakter, biedt tijdens de warmste zomeruren een deugddoende schaduwplek voor de tuinmannen en –vrouwen die in de moestuintjes werken.  


Op 22.09.18 – 23.09.18 nodigt CIVA je samen met Wood Wide Web uit om deze prachtige boom te (her)ontdekken, naast de rest van het aanbod van achttien uitzonderlijke tuinen verspreid over het hele Brusselse Gewest."
