A web site for Wood Wide Web



[![pipeline status](https://gitlab.com/atelier-cartographique/wood-wide-web/badges/master/pipeline.svg)](https://gitlab.com/atelier-cartographique/wood-wide-web/commits/master)



python3 deps:

```
pip install Pillow pybars3 networkx svgwrite markdown shapely
```

node deps:
```
npm i less postcss-cli autoprefixer
```


rsync
```
rsync -avhP target/  root@woodwideweb.be:/root/2018/
```

maybe  test  (--whole-file)
```
rsync -avhWP target/  root@woodwideweb.be:/root/2018/

```